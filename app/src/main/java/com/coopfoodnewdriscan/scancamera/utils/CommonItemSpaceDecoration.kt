package com.coopfoodnewdriscan.scancamera.utils

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration

class CommonItemSpaceDecoration(private val space: Int) : ItemDecoration() {
    private var mVerticalOrientation = true

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        outRect.top = CommonUtils.dp2px(view.context, space.toFloat())
        if (mVerticalOrientation) {
            if (parent.getChildAdapterPosition(view) == 0) {
                outRect[0, CommonUtils.dp2px(view.context, space.toFloat()), 0] =
                    CommonUtils.dp2px(view.context, space.toFloat())
            } else {
                outRect[0, 0, 0] = CommonUtils.dp2px(view.context, space.toFloat())
            }
        } else {
            if (parent.getChildAdapterPosition(view) == 0) {
                outRect[CommonUtils.dp2px(view.context, space.toFloat()), 0, 0] = 0
            } else {
                outRect[CommonUtils.dp2px(view.context, space.toFloat()), 0, CommonUtils.dp2px(
                    view.context,
                    space.toFloat()
                )] =
                    0
            }
        }
    }
}