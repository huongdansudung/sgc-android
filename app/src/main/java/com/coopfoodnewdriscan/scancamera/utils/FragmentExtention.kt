package com.coopfoodnewdriscan.scancamera.utils

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

fun Fragment.addFragmentToFragment(
    @IdRes containerViewId: Int,
    fragment: Fragment,
    addToBackStack: Boolean = false,
    tag: String = fragment::class.java.simpleName
) {
    val transaction: FragmentTransaction = fragmentManager!!.beginTransaction()
    transaction.add(containerViewId, fragment, fragment.javaClass.simpleName)
    if (addToBackStack) {
        transaction.addToBackStack(tag)
    }
    transaction.show(fragment)
    transaction.commit()
}

fun Fragment.replaceFragmentToFragment(
    @IdRes containerViewId: Int,
    fragment: Fragment,
    addToBackStack: Boolean = false,
    tag: String = fragment::class.java.simpleName
) {
    val transaction: FragmentTransaction = fragmentManager!!.beginTransaction()
    transaction.replace(containerViewId, fragment, fragment.javaClass.simpleName)
    if (addToBackStack) {
        transaction.addToBackStack(tag)
    }
    transaction.show(fragment)
    transaction.commit()
}

fun Fragment.removeFragment(rootTag: String) {
    fragmentManager!!.popBackStack(rootTag, FragmentManager.POP_BACK_STACK_INCLUSIVE)
}

fun Fragment.hideKeyboard() {
    val view = activity?.findViewById<View>(android.R.id.content)
    if (view != null) {
        val inputMethodManager =
            activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
