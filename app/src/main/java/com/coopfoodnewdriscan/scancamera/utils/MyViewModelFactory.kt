package com.coopfoodnewdriscan.scancamera.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.coopfoodnewdriscan.scancamera.data.source.PhotoRepository
import com.coopfoodnewdriscan.scancamera.screen.admin.AdminViewModel
import com.coopfoodnewdriscan.scancamera.screen.camera.CameraViewModel
import com.coopfoodnewdriscan.scancamera.screen.login.LoginViewModel
import com.coopfoodnewdriscan.scancamera.screen.selectoption.SelectOptionViewModel
import com.coopfoodnewdriscan.scancamera.screen.statistical.StatisticalViewModel

@Suppress("UNCHECKED_CAST")
class MyViewModelFactory(private val photoRepository: PhotoRepository) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when (modelClass) {
            LoginViewModel::class.java -> LoginViewModel(photoRepository) as T
            CameraViewModel::class.java -> CameraViewModel(photoRepository) as T
            StatisticalViewModel::class.java -> StatisticalViewModel(photoRepository) as T
            SelectOptionViewModel::class.java -> SelectOptionViewModel(photoRepository) as T
            AdminViewModel::class.java -> AdminViewModel(photoRepository) as T
            else -> super.create(modelClass)
        }
    }
}
