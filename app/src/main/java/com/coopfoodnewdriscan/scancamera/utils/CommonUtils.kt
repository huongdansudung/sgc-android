package com.coopfoodnewdriscan.scancamera.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ExifInterface
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Environment
import android.util.Log
import com.bumptech.glide.load.resource.bitmap.TransformationUtils
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

object CommonUtils {
    var isReport = false
    var TEXT_VIEW_COMPAPY = " "
    var IDUP = ""
    var TYPEBILL = ""
    var isDialogTimeout = false
    const val ACTION_USER = "ACTION_USER"
    const val EXTRA_USER = "EXTRA_USER"
    const val EXTRA_TYPE_BILL = "EXTRA_TYPE_BILL"
    const val VALUE_ONE_NUMBER = 1
    const val EXTRA_PHOTO_UPLOAD = "EXTRA_PHOTO_UPLOAD"
    const val EXTRS_PHOTO_UPLOAD_LIST = "EXTRS_PHOTO_UPLOAD_LIST"
    const val EXTRAS_POSITION = "EXTRAS_POSITION"
    const val EXTRA_IBATCH = "EXTRA_IBATCH"
    const val EXTRA_IPHOTO = "EXTRA_IPHOTO"
    const val EXTRA_BILLS = "EXTRA_BILLS"
    const val TIME_OUT_USER_DEFAULT = 1800
//    var hours: Int = 0
//    var minute: Int = 0
//    var seconds: Int = 0
    var year: Int = 0
    var month: Int = 0
    var day: Int = 0
    var isCountTimeOut = true
    var timeoutUser = 1800

    var timeDifference = 0L

    // isReUpload = false thì service được chạy và ngược lại isReUpload = true thì upload tay được chạy
    var isReUploadService = false
    var isReUploadHand = false

    fun navigationFile(coopBranch: String): String{
        return if (coopBranch == "CoopFood") {
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).toString() + "/" + "CoopFood/"
        }else {
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).toString() + "/" + "CoopMart/"
        }
    }

    fun getServerDatetime(simpleDateFormat: String): String {
        var currentTime = Calendar.getInstance().time.time
        currentTime -= timeDifference
        val array = SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.getDefault()).format(Date(currentTime)).split("-")
        year = array[0].toInt()
        month = array[1].toInt()
        day = array[2].toInt()
        when (simpleDateFormat) {
            "yyyyMMdd" -> {
                return String.format("%04d", array[0].toInt()) +
                        String.format("%02d", array[1].toInt()) +
                        String.format("%02d", array[2].toInt())
            }
            "yyyyMMddHHmmss" -> {
                return String.format("%04d", array[0].toInt()) +
                        String.format("%02d", array[1].toInt()) +
                        String.format("%02d", array[2].toInt()) +
                        String.format("%02d", array[3].toInt()) +
                        String.format("%02d", array[4].toInt()) +
                        String.format("%02d", array[5].toInt())
            }
            "yyyyMMdd_HHmmss" -> {
                return String.format("%04d", array[0].toInt()) +
                        String.format("%02d", array[1].toInt()) +
                        String.format("%02d", array[2].toInt()) + "_" +
                        String.format("%02d", array[3].toInt()) +
                        String.format("%02d", array[4].toInt()) +
                        String.format("%02d", array[5].toInt())
            }
            "yyyy/MM/dd" -> {
                return String.format("%04d", array[0].toInt()) + "/" +
                        String.format("%02d", array[1].toInt()) + "/" +
                        String.format("%02d", array[2].toInt())
            }
            else -> {
                Log.d("AAA", "simple Date Format wrong")
                return "simple Date Format wrong"
            }
        }
    }

    fun dp2px(context: Context, dpValue: Float): Int {
        val scale = context.resources.displayMetrics.density
        return (dpValue * scale + 0.5f).toInt()
    }

    fun isConnectionInternet(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)?.state == NetworkInfo.State.CONNECTED || connectivityManager.getNetworkInfo(
            ConnectivityManager.TYPE_WIFI
        )?.state == NetworkInfo.State.CONNECTED
    }

    fun photoRotation(photoFile: String): File {
        val file = File(photoFile)
        val bitmapData =
            getFileDataFromDrawable(file, BitmapFactory.decodeFile(file.absolutePath))
        var fileOutputStream: FileOutputStream? = null
        try {
            fileOutputStream = FileOutputStream(file)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        try {
            fileOutputStream!!.write(bitmapData)
            fileOutputStream.flush()
            fileOutputStream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file
    }

    private fun getFileDataFromDrawable(file: File, bitmap: Bitmap): ByteArray {
        val selectedBitmap: Bitmap
        val exifInterface = ExifInterface(file.absolutePath)
        val orientation = exifInterface.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )
        Log.d("AAAA", "getFileDataFromDrawable: " + orientation)
        selectedBitmap = when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> TransformationUtils.rotateImage(bitmap, 90)
            ExifInterface.ORIENTATION_ROTATE_180 -> TransformationUtils.rotateImage(bitmap, 180)
            ExifInterface.ORIENTATION_ROTATE_270 -> TransformationUtils.rotateImage(bitmap, 270)
            ExifInterface.ORIENTATION_NORMAL -> bitmap
            else -> bitmap
        }
        val result = Bitmap.createScaledBitmap(selectedBitmap, 1920, 2560, false)
        val byteArrayOutputStream = ByteArrayOutputStream()
        result.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream)
        return byteArrayOutputStream.toByteArray()
    }
}
