package com.coopfoodnewdriscan.scancamera.utils.custom

interface OnAnimationChangeListener {
    fun onScale(scaleFactor: Float, focalX: Float, focalY: Float)

    fun getScale(): Float

    fun onDrag(dx: Float, dy: Float)
}
