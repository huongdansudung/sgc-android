package com.coopfoodnewdriscan.scancamera.utils

interface OnItemRecyclerViewClickListener<T> {
    fun onItemClick(data: T)
}
