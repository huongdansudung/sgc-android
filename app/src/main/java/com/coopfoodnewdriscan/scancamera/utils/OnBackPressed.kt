package com.coopfoodnewdriscan.scancamera.utils

interface OnBackPressed {
    fun onBackPressed()
}