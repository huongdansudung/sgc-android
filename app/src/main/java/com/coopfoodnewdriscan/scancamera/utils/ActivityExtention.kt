package com.coopfoodnewdriscan.scancamera.utils

import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

fun AppCompatActivity.addFragmentToActivity(
    @IdRes containerViewId: Int,
    fragment: Fragment,
    addToBackStack: Boolean = false,
    tag: String = fragment::class.java.simpleName
) {
    val transaction = supportFragmentManager.beginTransaction()
    transaction.add(containerViewId, fragment, tag)
    if (addToBackStack) {
        transaction.addToBackStack(tag)
    }
    transaction.show(fragment)
    transaction.commit()
}

fun AppCompatActivity.removeFragment(rootTag: String) {
    supportFragmentManager.popBackStack(rootTag, FragmentManager.POP_BACK_STACK_INCLUSIVE)
}
