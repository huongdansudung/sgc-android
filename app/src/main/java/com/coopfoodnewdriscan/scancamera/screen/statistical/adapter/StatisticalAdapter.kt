package com.coopfoodnewdriscan.scancamera.screen.statistical.adapter

import android.view.View
import com.coopfoodnewdriscan.scancamera.R
import com.coopfoodnewdriscan.scancamera.data.model.TypeBill
import com.coopfoodnewdriscan.scancamera.utils.BaseAdapter
import com.coopfoodnewdriscan.scancamera.utils.BaseViewHolder
import com.coopfoodnewdriscan.scancamera.utils.OnItemRecyclerViewClickListener
import kotlinx.android.synthetic.main.activity_statistical.view.textViewTotalPhoto
import kotlinx.android.synthetic.main.activity_statistical.view.textViewTypeBill
import kotlinx.android.synthetic.main.item_recyclerview_statistical.view.*

class StatisticalAdapter : BaseAdapter<TypeBill>() {
    override fun layout(row: Int): Int = R.layout.item_recyclerview_statistical

    override fun viewHolder(view: View): BaseViewHolder<TypeBill> = StatisticalHolder(view)

    companion object {
        class StatisticalHolder(
            private val view: View
        ) : BaseViewHolder<TypeBill>(view) {

            override fun bindData(
                data: TypeBill,
                listener: OnItemRecyclerViewClickListener<TypeBill>,
                position: Int
            ) {
                view.textViewTypeBill.text = data.nameHD
                view.textViewTotalPhoto.text = data.totalPhoto.toString()
                view.textViewNumericalOrder.text = position.toString()
                view.setOnClickListener { listener.onItemClick(data) }
            }
        }
    }
}
