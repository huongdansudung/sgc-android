package com.coopfoodnewdriscan.scancamera.screen.selectbill

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.DividerItemDecoration
import com.coopfoodnewdriscan.scancamera.R
import com.coopfoodnewdriscan.scancamera.data.model.TypeBill
import com.coopfoodnewdriscan.scancamera.data.model.User
import com.coopfoodnewdriscan.scancamera.data.source.PhotoRepository
import com.coopfoodnewdriscan.scancamera.data.source.local.PhotoLocalDataSource
import com.coopfoodnewdriscan.scancamera.data.source.local.SharedPrefsApi
import com.coopfoodnewdriscan.scancamera.data.source.remote.PhotoRemoteDataSource
import com.coopfoodnewdriscan.scancamera.screen.camera.CameraActivity
import com.coopfoodnewdriscan.scancamera.screen.home.MainActivity
import com.coopfoodnewdriscan.scancamera.screen.login.LoginViewModel
import com.coopfoodnewdriscan.scancamera.screen.selectbill.adapter.SelectBillAdapter
import com.coopfoodnewdriscan.scancamera.utils.*
import com.google.android.gms.vision.barcode.Barcode
import com.google.gson.Gson
import com.notbytes.barcode_reader.BarcodeReaderActivity
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.fragment_select_bill.*
import java.io.File
import java.util.*

class SelectBillFragment : Fragment(), OnItemRecyclerViewClickListener<TypeBill>, OnBackPressed {
    private lateinit var viewModel: LoginViewModel
    private lateinit var user: User
    private lateinit var typeBill: TypeBill
    private lateinit var progressDialog: AlertDialog
    private lateinit var dialog: Dialog
    private lateinit var bills: List<TypeBill>
    private val selectBillAdapter: SelectBillAdapter by lazy { SelectBillAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_select_bill, container, false)
        setHasOptionsMenu(true)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initData()
        registerLiveData()
    }

    override fun onResume() {
        super.onResume()
        for (typeBill in bills) {
            bills[bills.indexOf(typeBill)].totalPhoto = 0
            val dateTimeNow = CommonUtils.getServerDatetime("yyyyMMddHHmmss")
            val file = File(CommonUtils.navigationFile(user.coopBranch))
            if (file.exists()) {
                val files = file.listFiles()
                if (files != null) {
                    for (file in files) {
                        val arrayName = file.name.split("_")
                        val timePhotoEnd = file.name.replace("_", "").substring(0, 14).toDouble()
                        val timeNow = dateTimeNow.toDouble()
                        if ((arrayName[4] == typeBill.idHD) && ((timeNow - timePhotoEnd) > 500)) {
                            bills[bills.indexOf(typeBill)].totalPhoto++
                        }
                    }
                }
            } else {
                bills[bills.indexOf(typeBill)].totalPhoto = 0
            }
        }
        selectBillAdapter.replaceItem(bills)
        recyclerViewSelectBill.adapter = null
        recyclerViewSelectBill.adapter = selectBillAdapter
    }

    override fun onBackPressed() {
        removeFragment(SelectBillFragment::class.java.simpleName)
    }

    override fun onItemClick(data: TypeBill) {
        typeBill = data
        CommonUtils.TYPEBILL = data.nameHD
        if (data.idHD == "9" || data.idHD == "12") {
            val launchIntent = BarcodeReaderActivity.getLaunchIntent(context, true, false)
            startActivityForResult(launchIntent, BARCODE_READER_ACTIVITY_REQUEST)
        } else {
            if (CommonUtils.isConnectionInternet(activity!!.applicationContext)) {
                progressDialog.show()
                Log.d("log",  "" + CommonUtils.getServerDatetime("yyyyMMdd") + "/" + data.idHD)
                viewModel.getBatch(CommonUtils.getServerDatetime("yyyyMMdd"), data.idHD)
            } else {
                Toast.makeText(
                    context,
                    resources.getString(R.string.message_internet),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) {
            Toast.makeText(
                context,
                resources.getString(R.string.error_in_scanning),
                Toast.LENGTH_SHORT
            ).show()
            return
        }
        if (requestCode == BARCODE_READER_ACTIVITY_REQUEST && data != null) {
            val barcode =
                data.getParcelableExtra<Barcode>(BarcodeReaderActivity.KEY_CAPTURED_BARCODE)
            val resultData = barcode!!.rawValue
            val arrayReport =
                resultData.split("\\|".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            if (arrayReport.size == 5) {
                if (arrayReport[0] == "sgcoop" && arrayReport[1] == user.idCus_new && arrayReport[2] == "bienban" && CommonUtils.TYPEBILL == "Đính kèm biên bản") {
                    val typeBill = TypeBill("9", "Đính kèm biên bản", 0)
                    CommonUtils.TEXT_VIEW_COMPAPY = arrayReport[4]
                    CommonUtils.IDUP = arrayReport[3]
                    startActivity(CameraActivity.instance(context!!, user, typeBill, "0", "0"))
                    return
                } else if (arrayReport[0] == "sgcoop" && arrayReport[1] == user.idCus_new && arrayReport[2] == "HD" && CommonUtils.TYPEBILL == "Đính kèm hóa đơn") {
                    val typeBill = TypeBill("12", "Đính kèm hóa đơn", 0)
                    CommonUtils.TEXT_VIEW_COMPAPY = arrayReport[4]
                    CommonUtils.IDUP = arrayReport[3]
                    startActivity(CameraActivity.instance(context!!, user, typeBill, "0", "0"))
                    return
                }
            }
            Toast.makeText(context, resources.getString(R.string.error_QR), Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun initView() {
        dialog = Dialog(context!!)
        dialog.setContentView(R.layout.custom_dialog_update_app)
        progressDialog = SpotsDialog.Builder()
            .setContext(context)
            .setCancelable(false)
            .setTheme(R.style.CustomerProgress)
            .build()
        user = arguments!!.getParcelable(CommonUtils.ACTION_USER)!!
        bills = arguments!!.getParcelableArrayList(CommonUtils.EXTRA_BILLS)!!
        getOutputDirectory(context!!, user)
        val dividerItemDecoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        recyclerViewSelectBill.addItemDecoration(dividerItemDecoration)
        recyclerViewSelectBill.setHasFixedSize(true)
        selectBillAdapter.replaceItem(bills)
        recyclerViewSelectBill.adapter = selectBillAdapter
        selectBillAdapter.setOnItemClickListener(this)
    }

    private fun initData() {
        val photoRepository = PhotoRepository.instance(
            PhotoLocalDataSource.instance(SharedPrefsApi(context!!, Gson())),
            PhotoRemoteDataSource.instance(SharedPrefsApi(context!!, Gson()))
        )
        viewModel = ViewModelProviders.of(this, MyViewModelFactory(photoRepository))
            .get(LoginViewModel::class.java)
    }

    private fun registerLiveData() {
        viewModel.batchLiveData.observe(this, Observer {
            if (it.message == "yes") {
                progressDialog.dismiss()
                dialog.setCancelable(false)
                dialog.window!!.setBackgroundDrawable(ColorDrawable(0))
                dialog.findViewById<ImageView>(R.id.imageViewCancel)
                    .setOnClickListener { activity!!.finish() }
                dialog.findViewById<FrameLayout>(R.id.buttonUpdate).setOnClickListener {
                    val appPackageName = activity!!.packageName;
                    try {
                        startActivity(
                            Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                            )
                        )
                    } catch (e: Exception) {
                        Log.d("AAA", e.message!!)
                    }
                }
                dialog.show()
            } else {
                progressDialog.dismiss()
                val file = File(CommonUtils.navigationFile(user.coopBranch))
                val files = file.listFiles()
                var count = 0
                if (files != null) {
                    for (file in files) {
                        val arrayName = file.name.split("_")
                        if (arrayName[4] == typeBill.idHD &&
                            arrayName[7] == String.format("%02d", Integer.parseInt(it.batch)) &&
                            arrayName[0] == CommonUtils.getServerDatetime("yyyyMMdd")
                        ) {
                            count += 1
                        }
                    }
                }
                startActivity(
                    CameraActivity.instance(
                        context!!,
                        user,
                        typeBill,
                        it.batch,
                        (it.photoNumber.toInt() + count).toString()
                    )
                )
            }
        })
        viewModel.errorLiveData.observe(this, Observer {
            if (it.message!!.contains("HTTP 422 UNPROCESSABLE ENTITY")) {
                val intent = Intent(CameraActivity.KEY_EVENT_ACTION).apply {
                    putExtra(
                        CameraActivity.KEY_EVENT_EXTRA, 111
                    )
                }
                val intentAc = Intent(context!!, MainActivity::class.java)
                intentAc.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intentAc)
                Toast.makeText(
                    view!!.context,
                    resources.getString(R.string.message_refresh_token_expires),
                    Toast.LENGTH_LONG
                ).show()
                LocalBroadcastManager.getInstance(context!!).sendBroadcast(intent)
            } else {
                Toast.makeText(view!!.context, it!!.message, Toast.LENGTH_LONG).show()
            }
        })
    }

    companion object {
        private val BARCODE_READER_ACTIVITY_REQUEST = 1208

        fun getOutputDirectory(context: Context, user: User): File {
            val appContext = context.applicationContext
            val mediaDir = context.externalMediaDirs.firstOrNull()?.let {
                File(CommonUtils.navigationFile(user.coopBranch)).apply { mkdirs() }
            }
            return if (mediaDir != null && mediaDir.exists()) mediaDir else appContext.filesDir
        }

        fun instance(user: User, bills: List<TypeBill>) = SelectBillFragment().apply {
            val args = Bundle()
            args.putParcelable(CommonUtils.ACTION_USER, user)
            args.putParcelableArrayList(CommonUtils.EXTRA_BILLS, bills as ArrayList<TypeBill>)
            arguments = args
        }
    }
}
