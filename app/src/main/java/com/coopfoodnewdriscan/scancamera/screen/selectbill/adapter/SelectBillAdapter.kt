package com.coopfoodnewdriscan.scancamera.screen.selectbill.adapter

import android.graphics.Color
import android.view.View
import androidx.core.content.ContextCompat
import com.coopfoodnewdriscan.scancamera.R
import com.coopfoodnewdriscan.scancamera.data.model.TypeBill
import com.coopfoodnewdriscan.scancamera.utils.BaseAdapter
import com.coopfoodnewdriscan.scancamera.utils.BaseViewHolder
import com.coopfoodnewdriscan.scancamera.utils.OnItemRecyclerViewClickListener
import kotlinx.android.synthetic.main.item_recyclerview_select_bill.view.*

class SelectBillAdapter : BaseAdapter<TypeBill>() {
    override fun layout(row: Int): Int = R.layout.item_recyclerview_select_bill

    override fun viewHolder(view: View): BaseViewHolder<TypeBill> = SelectBillHolder(view)

    companion object {
        class SelectBillHolder(
            private val view: View
        ) : BaseViewHolder<TypeBill>(view) {

            override fun bindData(
                data: TypeBill,
                listener: OnItemRecyclerViewClickListener<TypeBill>,
                position: Int
            ) {
                if (data.idHD == "9" || data.idHD == "12"){
                    view.textViewNotPhotoUpload.text = ""
                }else{
                    view.textViewNotPhotoUpload.text = "(${data.totalPhoto})"
                    if(data.totalPhoto > 0){
                        view.background_bills.setBackgroundColor(ContextCompat.getColor(view.context, R.color.colorAccent))
                        view.textViewTypeBill.setTextColor(Color.WHITE)
                        view.textViewNotPhotoUpload.setTextColor(Color.WHITE)
                    }else {
                        view.background_bills.setBackgroundColor(ContextCompat.getColor(view.context, R.color.colorWhile))
                        view.textViewTypeBill.setTextColor(Color.BLACK)
                        view.textViewNotPhotoUpload.setTextColor(Color.BLACK)
                    }
                }
                view.textViewTypeBill.text = data.nameHD
                view.setOnClickListener { listener.onItemClick(data) }
            }
        }
    }
}
