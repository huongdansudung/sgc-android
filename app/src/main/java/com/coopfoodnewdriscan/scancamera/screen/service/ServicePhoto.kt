package com.coopfoodnewdriscan.scancamera.screen.service

import android.app.ActivityManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.coopfoodnewdriscan.scancamera.data.model.User
import com.coopfoodnewdriscan.scancamera.data.source.PhotoRepository
import com.coopfoodnewdriscan.scancamera.data.source.local.PhotoLocalDataSource
import com.coopfoodnewdriscan.scancamera.data.source.local.SharedPrefsApi
import com.coopfoodnewdriscan.scancamera.data.source.remote.PhotoRemoteDataSource
import com.coopfoodnewdriscan.scancamera.screen.camera.CameraActivity
import com.coopfoodnewdriscan.scancamera.screen.home.MainActivity
import com.coopfoodnewdriscan.scancamera.screen.notification.PhotoNotification
import com.coopfoodnewdriscan.scancamera.utils.CommonUtils
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import kotlin.math.log

class ServicePhoto : Service() {
    private lateinit var user: User
    private lateinit var photoRepository: PhotoRepository
    private val photoNotification: PhotoNotification by lazy { PhotoNotification(this) }
    private val compositeDisposable = CompositeDisposable()
    private val handler: Handler by lazy { Handler() }
    private val handlerCountTime: Handler by lazy { Handler() }
    private var coopBranch = ""
    private val myRunnable: Runnable = object : Runnable {
        override fun run() {
            if (CommonUtils.isCountTimeOut) {
                CommonUtils.timeoutUser--
                if (CommonUtils.timeoutUser <= 0) {
                    CommonUtils.isCountTimeOut = false
                    val intent = Intent(CameraActivity.KEY_EVENT_ACTION).apply {
                        putExtra(
                            CameraActivity.KEY_EVENT_EXTRA, 111
                        )
                    }
                    val intentAc = Intent(applicationContext, MainActivity::class.java)
                    intentAc.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intentAc)
                    LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
                    Toast.makeText(
                        applicationContext,
                        "Kết thúc phiên làm việc quá 30 phút!",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
            handlerCountTime.postDelayed(this, 1000)
        }
    }

    override fun onCreate() {
        super.onCreate()
        photoNotification.initNotification()
        startForeground(
            PhotoNotification.NOTIFICATION_INT_ID,
            photoNotification.getBuilder().build()
        )

    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        //select server datetime
        handlerCountTime.postDelayed(myRunnable, 0)

        //auto upload photo
        user = intent!!.getParcelableExtra(CommonUtils.EXTRA_USER)!!
        coopBranch = user.coopBranch

        photoRepository = PhotoRepository.instance(
            PhotoLocalDataSource.instance(SharedPrefsApi(applicationContext, Gson())),
            PhotoRemoteDataSource.instance(SharedPrefsApi(this, Gson()))
        )
        handler.postDelayed(object : Runnable {
            @RequiresApi(Build.VERSION_CODES.N)
            override fun run() {
                user = photoRepository.loadInformationAccount()
                if (coopBranch != user.coopBranch) {
                    photoRepository = PhotoRepository.instanceOption(
                        PhotoLocalDataSource.instance(SharedPrefsApi(applicationContext, Gson())),
                        PhotoRemoteDataSource.instanceOption(
                            SharedPrefsApi(
                                applicationContext,
                                Gson()
                            )
                        )
                    )
                }

                var countPhoto = 0
                val file = File(CommonUtils.navigationFile(user.coopBranch))

                if (file.exists()) {
                    if (!CommonUtils.isReUploadHand) {
                        var files = file.listFiles()
                        files = sortFiles(files!!)
                        if (files.isNotEmpty()) {
                            countPhoto += files.size
                            val thread = Thread(
                                Runnable {
                                    val dateTime =
                                        CommonUtils.getServerDatetime("yyyyMMddHHmmss")
                                    if (java.lang.Double.parseDouble(dateTime) - java.lang.Double.parseDouble(
                                            files[0].name.replace("_", "").substring(0, 14)
                                        ) > 150
                                    ) {
                                        try {
                                            CommonUtils.isReUploadService = true
                                            val requestFile = RequestBody.create(
                                                "multipart/form-data".toMediaTypeOrNull(),
                                                CommonUtils.photoRotation(files[0].absolutePath)
                                            )
                                            val body = MultipartBody.Part.createFormData(
                                                "image",
                                                files[0].name,
                                                requestFile
                                            )
                                            compositeDisposable.add(
                                                photoRepository.uploadFileName(files[0].name)
                                                    .subscribeOn(Schedulers.io())
                                                    .observeOn(Schedulers.trampoline())
                                                    .subscribe({ data ->
                                                        data.let {
                                                            compositeDisposable.add(
                                                                photoRepository.uploadFilePhoto(
                                                                    body,
                                                                    ""
                                                                )
                                                                    .subscribeOn(Schedulers.io())
                                                                    .observeOn(Schedulers.trampoline())
                                                                    .subscribe({ data ->
                                                                        data.let {
                                                                            File(
                                                                                CommonUtils.navigationFile(
                                                                                    user.coopBranch
                                                                                ) + it.message
                                                                            ).delete()
                                                                            CommonUtils.isReUploadService =
                                                                                false
                                                                            handler.postDelayed(
                                                                                this,
                                                                                2000
                                                                            )
                                                                        }
                                                                    }, { error ->
                                                                        error.let {
                                                                            handler.postDelayed(
                                                                                this,
                                                                                2000
                                                                            )
                                                                            Log.d(
                                                                                "Error upload service",
                                                                                error.message!!
                                                                            )
                                                                        }
                                                                    })
                                                            )
                                                        }
                                                    }, { error ->
                                                        error.let {
                                                            handler.postDelayed(this, 2000)
//                                                        Log.d(
//                                                            "Error upload service",
//                                                            error.message!!
//                                                        )
                                                        }
                                                    })
                                            )
                                        } catch (e: java.lang.Exception) {
                                            Log.d("TAG", "vào catch: ")
                                            files[0].delete()
                                            handler.postDelayed(this, 2000)
                                        }
                                    } else {
                                        handler.postDelayed(this, 90000)
                                    }
                                }
                            )
                            thread.start()
                        } else {
                            handler.postDelayed(this, 90000)
                        }
                    } else {
                        handler.postDelayed(this, 60000)
//                        Log.d("AAA", "Đang tiến hành úp ảnh bằng tay không chạy được service")
                    }
                } else {
                    handler.postDelayed(this, 60000)
                }
                Log.d("OkHttp:", "check service")
                if (countPhoto == 0 && !isRunning(this@ServicePhoto)) {
                    handlerCountTime.removeCallbacks(myRunnable)
                    handler.removeCallbacks(this)
                    photoNotification.getBuilder().setOngoing(false)
                    stopForeground(flags)
                    stopSelf()
                }
            }
        }, 0)
        return START_NOT_STICKY
    }

    private fun sortFiles(files: Array<File>): Array<File> {
        for (i in files.indices) {
            for (j in i + 1 until files.size) {
                val timeI = files[i].name.split("_")[0] + files[i].name.split("_")[1]
                val timeJ = files[j].name.split("_")[0] + files[j].name.split("_")[1]
                if (timeI < timeJ) {
                    val tamp = files[i]
                    files[i] = files[j]
                    files[j] = tamp
                }
            }
        }
        return files
    }

    fun isRunning(ctx: Context): Boolean {
        val activityManager = ctx.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val tasks = activityManager.getRunningTasks(Integer.MAX_VALUE)
        for (task in tasks) {
            if (ctx.packageName.equals(task.baseActivity!!.packageName, ignoreCase = true))
                return true
        }
        return false
    }

    companion object {
        fun serviceInstance(context: Context, user: User): Intent {
            val intent = Intent(context, ServicePhoto::class.java)
            intent.putExtra(CommonUtils.EXTRA_USER, user)
            return intent
        }
    }
}
