package com.coopfoodnewdriscan.scancamera.screen.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.coopfoodnewdriscan.scancamera.data.model.AccessToken
import com.coopfoodnewdriscan.scancamera.data.model.Batch
import com.coopfoodnewdriscan.scancamera.data.model.User
import com.coopfoodnewdriscan.scancamera.data.source.PhotoRepository
import com.coopfoodnewdriscan.scancamera.data.source.remote.response.InAppUpdateResponse
import com.coopfoodnewdriscan.scancamera.data.source.remote.response.PhotoUploadResponse
import com.coopfoodnewdriscan.scancamera.data.source.remote.response.UploadFilePhoto
import com.coopfoodnewdriscan.scancamera.utils.CommonUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import java.io.File

class LoginViewModel(private val photoRepository: PhotoRepository) : ViewModel() {
    var batchLiveData = MutableLiveData<Batch>()
    var photoUploadLiveData = MutableLiveData<PhotoUploadResponse>()
    var loginLiveData = MutableLiveData<User>()
    var errorLiveData = MutableLiveData<Throwable>()
    var reUploadFilePhoto = MutableLiveData<UploadFilePhoto>()
    var reUploadFileName = MutableLiveData<UploadFilePhoto>()
    var inAppUpdateLiveData = MutableLiveData<InAppUpdateResponse>()
    private val compositeDisposable = CompositeDisposable()

    fun inAppUpdate(version: String) {
        compositeDisposable.add(
            photoRepository.inAppUpdate(version)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ data ->
                    data.let { inAppUpdateLiveData.value = it }
                }, { error ->
                    error.let { errorLiveData.value = it }
                })
        )
    }

    fun login(username: String, password: String, version: String, deviceMac: String) {
        compositeDisposable.add(
            photoRepository.login(username, password, version, deviceMac)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ data ->
                    data.let { loginLiveData.value = it }
                }, { error ->
                    error.let { errorLiveData.value = it }
                })
        )
    }

    fun getBatch(date: String, typeInvoice: String) {
        compositeDisposable.add(
            photoRepository.getBatch(date, typeInvoice)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ data ->
                    data.let {
                        batchLiveData.value = it
                    }
                }, { error ->
                    error.let { errorLiveData.value = it }
                })
        )
    }

    fun getPhotoUpload(
        typeBill: String,
        datetime: String,
        batch: String,
        page: String
    ) {
        compositeDisposable.add(
            photoRepository.getPhotoUpload(typeBill, datetime, batch, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ data ->
                    data.let {
                        photoUploadLiveData.value = it
                    }
                }, { error ->
                    error.let { errorLiveData.value = it }
                })
        )
    }

    fun uploadFileName(filename: String) {
        compositeDisposable.add(
            photoRepository.uploadFileName(filename)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ data ->
                    data.let {
                        reUploadFileName.value = it
                    }
                }, { error ->
                    error.let {
                        errorLiveData.value = it
                    }
                })
        )
    }

    fun uploadFilePhoto(imageFile: MultipartBody.Part, groupHD: String) {
        val user = photoRepository.loadInformationAccount()
        compositeDisposable.add(
            photoRepository.uploadFilePhoto(imageFile, groupHD)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ data ->
                    data.let {
                        if (it.message != "Error") {
                            File(CommonUtils.navigationFile(user.coopBranch) + it.message).delete()
                        }
                        reUploadFilePhoto.postValue(it)
                    }
                }, { error ->
                    error.let { errorLiveData.postValue(it) }
                })
        )
    }

    fun saveInformationAccount(user: User, userName: String, password: String) {
        photoRepository.saveInformationAccount(user, userName, password)
    }

    fun loadInformationAccount(): User {
        return photoRepository.loadInformationAccount()
    }

    fun saveAccessToken(accessToken: AccessToken) {
        photoRepository.saveAccessToken(accessToken)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
