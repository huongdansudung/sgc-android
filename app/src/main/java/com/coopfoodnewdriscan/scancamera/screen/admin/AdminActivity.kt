package com.coopfoodnewdriscan.scancamera.screen.admin

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.coopfoodnewdriscan.scancamera.R
import com.coopfoodnewdriscan.scancamera.data.model.User
import com.coopfoodnewdriscan.scancamera.data.model.UserAdmin
import com.coopfoodnewdriscan.scancamera.data.source.PhotoRepository
import com.coopfoodnewdriscan.scancamera.data.source.local.PhotoLocalDataSource
import com.coopfoodnewdriscan.scancamera.data.source.local.SharedPrefsApi
import com.coopfoodnewdriscan.scancamera.data.source.remote.PhotoRemoteDataSource
import com.coopfoodnewdriscan.scancamera.screen.admin.adapter.AdminAdapter
import com.coopfoodnewdriscan.scancamera.screen.camera.CameraActivity
import com.coopfoodnewdriscan.scancamera.screen.home.MainActivity
import com.coopfoodnewdriscan.scancamera.utils.CommonItemSpaceDecoration
import com.coopfoodnewdriscan.scancamera.utils.CommonUtils
import com.coopfoodnewdriscan.scancamera.utils.MyViewModelFactory
import com.coopfoodnewdriscan.scancamera.utils.OnItemRecyclerViewClickListener
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_admin.*

class AdminActivity : AppCompatActivity(),
    OnItemRecyclerViewClickListener<UserAdmin> {
    private val adminAdapter: AdminAdapter by lazy { AdminAdapter() }
    private lateinit var viewModel: AdminViewModel
    private lateinit var dialog: Dialog
    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)
        initView()
        initData()
        registerLiveData()
    }

    private fun initView() {
        dialog = Dialog(this)
        dialog.setContentView(R.layout.custom_dialog_timeout)
        recyclerViewAdminUser.addItemDecoration(CommonItemSpaceDecoration(16))
        recyclerViewAdminUser.adapter = adminAdapter
        adminAdapter.setOnItemClickListener(this)
    }

    private fun initData() {
        val photoLocalDataSource =
            PhotoLocalDataSource.instance(SharedPrefsApi(applicationContext, Gson()))
        user = photoLocalDataSource.loadInformationAccount()
        val photoRepository = PhotoRepository.instanceOption(
            photoLocalDataSource,
            PhotoRemoteDataSource.instanceOption(SharedPrefsApi(this, Gson()))
        )
        viewModel = ViewModelProviders.of(this, MyViewModelFactory(photoRepository)).get(
            AdminViewModel::class.java
        )
        viewModel.userAdmin()
    }

    private fun registerLiveData() {
        viewModel.errorLiveData.observe(this, Observer {
            if (it.message!!.contains("HTTP 422 UNPROCESSABLE ENTITY")){
                val intent = Intent(CameraActivity.KEY_EVENT_ACTION).apply { putExtra(
                    CameraActivity.KEY_EVENT_EXTRA, 111) }
                val intentAc = Intent(applicationContext, MainActivity::class.java)
                intentAc.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intentAc)
                Toast.makeText(applicationContext,  resources.getString(R.string.message_refresh_token_expires), Toast.LENGTH_LONG).show()
                LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
            }else {
                Toast.makeText(applicationContext, it!!.message, Toast.LENGTH_LONG).show()
            }
        })
        viewModel.resetMacLiveData.observe(this, Observer {
            progressView.visibility = View.GONE
            if (it.message == "Success") {
                dialog.setContentView(R.layout.custom_dialog_result)
                dialog.setCancelable(false)
                dialog.findViewById<TextView>(R.id.textViewTitle).text = "Làm mới thành công"
                dialog.findViewById<TextView>(R.id.textViewMessage).text =
                    "Bạn có muốn logout hay không ?"
                dialog.findViewById<ImageView>(R.id.imageViewIconTick)
                    .setImageDrawable(resources.getDrawable(R.drawable.tick_green))
                dialog.window!!.setBackgroundDrawable(ColorDrawable(0))
                dialog.findViewById<Button>(R.id.buttonOK).setOnClickListener {
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }
                dialog.findViewById<Button>(R.id.buttonCancel)
                    .setOnClickListener { dialog.cancel() }
                dialog.show()
            } else {
                dialog.setCancelable(false)
                dialog.findViewById<TextView>(R.id.textViewTitle).text = "Làm mới thất bại"
                dialog.findViewById<TextView>(R.id.textViewMessage).text = "Vui lòng thử lại !"
                dialog.findViewById<ImageView>(R.id.imageViewIconTick)
                    .setImageDrawable(resources.getDrawable(R.drawable.icon_error))
                dialog.window!!.setBackgroundDrawable(ColorDrawable(0))
                dialog.findViewById<Button>(R.id.buttonOK).setOnClickListener { dialog.cancel() }
                dialog.show()
            }
        })
        viewModel.userAdminLiveData.observe(this, Observer {
            if (it.message == "OK"){
                adminAdapter.replaceItem(it.listUser)
            }
        })
    }

    override fun onItemClick(data: UserAdmin) {
        val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
            when (which) {
                DialogInterface.BUTTON_POSITIVE -> {
                    if (CommonUtils.isConnectionInternet(applicationContext.applicationContext)) {
                        viewModel.resetMac(data.userId)
                        constraintLayoutAdmin.visibility = View.VISIBLE
                        progressView.visibility = View.VISIBLE
                    } else {
                        Toast.makeText(
                            applicationContext.applicationContext,
                            resources.getString(R.string.message_internet),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

                DialogInterface.BUTTON_NEGATIVE -> {
                }
            }
        }
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Làm mới login cho ${data.userName}?")
            .setPositiveButton("Có", dialogClickListener)
            .setNegativeButton("Không", dialogClickListener).show()
    }

    companion object {
        fun instance(context: Context): Intent {
            return Intent(context, AdminActivity::class.java)
        }
    }
}
