package com.coopfoodnewdriscan.scancamera.screen.home

import android.Manifest
import android.content.*
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.coopfoodnewdriscan.scancamera.R
import com.coopfoodnewdriscan.scancamera.screen.camera.CameraActivity.Companion.KEY_EVENT_ACTION
import com.coopfoodnewdriscan.scancamera.screen.camera.CameraActivity.Companion.KEY_EVENT_EXTRA
import com.coopfoodnewdriscan.scancamera.screen.login.LoginFragment
import com.coopfoodnewdriscan.scancamera.screen.selectbill.SelectBillFragment
import com.coopfoodnewdriscan.scancamera.screen.selectoption.SelectOptionFragment
import com.coopfoodnewdriscan.scancamera.utils.CommonUtils
import com.coopfoodnewdriscan.scancamera.utils.addFragmentToActivity
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.DexterError
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.PermissionRequestErrorListener
import com.karumi.dexter.listener.multi.MultiplePermissionsListener

class MainActivity : AppCompatActivity() , MultiplePermissionsListener,
    PermissionRequestErrorListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
            )
            .withListener(this)
            .withErrorListener(this)
            .onSameThread()
            .check()
        val filter = IntentFilter().apply { addAction(KEY_EVENT_ACTION) }
        LocalBroadcastManager.getInstance(applicationContext).registerReceiver(volumeDownReceiver, filter)
        addFragmentToActivity(R.id.layoutContainer, LoginFragment.instance())
    }

    override fun onDestroy() {
        super.onDestroy()
        CommonUtils.isDialogTimeout = false
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        CommonUtils.timeoutUser = CommonUtils.TIME_OUT_USER_DEFAULT
    }

    private val volumeDownReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                when (intent.getIntExtra(KEY_EVENT_EXTRA, 111)) {
                    111 -> {
                        CommonUtils.isCountTimeOut = false
                        finish()
                    }
                }
            }
    }

    override fun onBackPressed() {
        tellFragments()
    }

    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
        if (report!!.areAllPermissionsGranted()) {

        }


        if (report.isAnyPermissionPermanentlyDenied) {

        }
    }

    override fun onPermissionRationaleShouldBeShown(
        permissions: MutableList<PermissionRequest>?,
        token: PermissionToken?
    ) {

    }

    override fun onError(error: DexterError?) {
        Toast.makeText(
            applicationContext,
            "Error occurred! $error",
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun tellFragments() {
        val fragments = supportFragmentManager.fragments
        for (f in fragments) {
            if (f != null && f is SelectBillFragment)
                f.onBackPressed()
            if (f != null && f is SelectOptionFragment) {
                finish()
            }
            if (f != null && f is LoginFragment) {
                finish()
            }
        }
    }
}
