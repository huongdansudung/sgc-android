package com.coopfoodnewdriscan.scancamera.screen.photodetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.coopfoodnewdriscan.scancamera.data.model.PhotoUpload
import com.coopfoodnewdriscan.scancamera.utils.CommonUtils
import com.coopfoodnewdriscan.scancamera.utils.custom.HandleZoomEvent
import com.coopfoodnewdriscan.scancamera.utils.loadImageUrl

class PhotoFragment internal constructor() : Fragment(), View.OnClickListener {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = ImageView(context)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args = arguments ?: return
        val resource = args.getParcelable<PhotoUpload>(FILE_NAME_KEY)
        (view as ImageView).loadImageUrl(resource!!.url)
        HandleZoomEvent(view)
    }

    override fun onClick(v: View?) {
        CommonUtils.timeoutUser = CommonUtils.TIME_OUT_USER_DEFAULT
    }

    companion object {
        private const val FILE_NAME_KEY = "file_name"

        fun create(photoUpload: PhotoUpload) = PhotoFragment().apply {
            arguments = Bundle().apply { putParcelable(FILE_NAME_KEY, photoUpload) }
        }
    }
}
