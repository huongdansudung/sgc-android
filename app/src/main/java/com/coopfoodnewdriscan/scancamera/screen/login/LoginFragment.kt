package com.coopfoodnewdriscan.scancamera.screen.login

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.coopfoodnewdriscan.scancamera.R
import com.coopfoodnewdriscan.scancamera.data.model.AccessToken
import com.coopfoodnewdriscan.scancamera.data.source.PhotoRepository
import com.coopfoodnewdriscan.scancamera.data.source.local.PhotoLocalDataSource
import com.coopfoodnewdriscan.scancamera.data.source.local.SharedPrefsApi
import com.coopfoodnewdriscan.scancamera.data.source.remote.PhotoRemoteDataSource
import com.coopfoodnewdriscan.scancamera.data.source.remote.api.ServiceGenerator
import com.coopfoodnewdriscan.scancamera.screen.admin.AdminActivity
import com.coopfoodnewdriscan.scancamera.screen.selectoption.SelectOptionFragment
import com.coopfoodnewdriscan.scancamera.utils.CommonUtils
import com.coopfoodnewdriscan.scancamera.utils.MyViewModelFactory
import com.coopfoodnewdriscan.scancamera.utils.hideKeyboard
import com.coopfoodnewdriscan.scancamera.utils.replaceFragmentToFragment
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : Fragment(), View.OnClickListener, View.OnFocusChangeListener {
    private var version = "2.0.0"
    private lateinit var viewModel: LoginViewModel
    private lateinit var dialog: Dialog

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initData()
        registerLiveData()
    }

    override fun onResume() {
        super.onResume()
        val user = viewModel.loadInformationAccount()
        editTextUsername.setText(user.userName)
        editTextPassword.setText(user.password)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.buttonLogin -> {
                animatorButtonLogin()
                hideKeyboard()
                login()
            }
            R.id.imageViewVersion -> {
                showDialog("Nội dung cập nhật!")
            }
        }
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        when (v?.id) {
            R.id.editTextUsername -> {
                if (!hasFocus) buttonLogin.isEnabled = true
            }
            R.id.editTextPassword -> {
                if (!hasFocus) buttonLogin.isEnabled = true
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initView() {
        ServiceGenerator.changeApiBaseUrl("http://210.2.93.45:5020/")
        dialog = Dialog(context!!)
        dialog.setContentView(R.layout.custom_dialog_update_app)
        textViewVersion.text = "Version: 2.0.0"
        inputLayoutUserName.hint = resources.getString(R.string.title_username)
        inputLayoutPassword.hint = resources.getString(R.string.title_password)
        buttonLogin.setOnClickListener(this)
        imageViewVersion.setOnClickListener(this)
        editTextUsername.onFocusChangeListener = this
        editTextPassword.onFocusChangeListener = this
    }

    private fun initData() {
        val photoRepository = PhotoRepository.instanceOption(
            PhotoLocalDataSource.instance(SharedPrefsApi(context!!, Gson())),
            PhotoRemoteDataSource.instanceOption(SharedPrefsApi(context!!, Gson()))
        )
        viewModel = ViewModelProviders.of(this, MyViewModelFactory(photoRepository))
            .get(LoginViewModel::class.java)
        viewModel.inAppUpdate(version)
    }

    private fun registerLiveData() {
        viewModel.loginLiveData.observe(this, Observer {
            val handler = Handler()
            handler.postDelayed(object : Runnable {
                override fun run() {
                    if (it.message == "version new") {
                        resetButtonLogin()
                        dialog.setCancelable(false)
                        dialog.window!!.setBackgroundDrawable(ColorDrawable(0))
                        dialog.findViewById<ImageView>(R.id.imageViewCancel)
                            .setOnClickListener { activity!!.finish() }
                        dialog.findViewById<FrameLayout>(R.id.buttonUpdate).setOnClickListener {
                            val appPackageName = activity!!.packageName
                            try {
                                startActivity(
                                    Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                                    )
                                )
                            } catch (e: Exception) {
                                Log.d("AAA", e.message!!)
                            }
                        }
                        dialog.show()
                    } else {
                        if (it.message == "Access Denied") {
                            resetButtonLogin()
                            Toast.makeText(
                                context,
                                resources.getString(R.string.message_device_mac),
                                Toast.LENGTH_LONG
                            ).show()
                        } else if (!it.message.contains("Auth successful")) {
                            resetButtonLogin()
                            Toast.makeText(
                                context,
                                resources.getString(R.string.login_fail),
                                Toast.LENGTH_LONG
                            ).show()
                        } else {
                            val accessToken = AccessToken(it.token, it.refreshToken, it.message)
                            viewModel.saveAccessToken(accessToken)
                            if (it.coopBranch == "CoopFood") {
                                ServiceGenerator.changeApiBaseUrl("http://210.2.93.45:5015/")
                            }
                            viewModel.saveInformationAccount(
                                it,
                                editTextUsername.text.toString(),
                                editTextPassword.text.toString()
                            )
                            if (it.role == "1") {
                                val user = it
                                user.userName = editTextUsername.text.toString()
                                user.password = editTextPassword.text.toString()
                                CommonUtils.isCountTimeOut = true
                                CommonUtils.timeoutUser = CommonUtils.TIME_OUT_USER_DEFAULT
                                replaceFragmentToFragment(
                                    R.id.layoutContainer,
                                    SelectOptionFragment.instance(user),
                                    true
                                )
                            } else if (it.role == "MNG") {
                                CommonUtils.isCountTimeOut = false
                                startActivity(AdminActivity.instance(context!!))
                                activity!!.finish()
                            }
                        }
                    }
                }
            }, 80)
        })
        viewModel.inAppUpdateLiveData.observe(this, Observer {
            if (it.message == "version new") {
                dialog.setCancelable(false)
                dialog.window!!.setBackgroundDrawable(ColorDrawable(0))
                dialog.findViewById<ImageView>(R.id.imageViewCancel)
                    .setOnClickListener { activity!!.finish() }
                dialog.findViewById<FrameLayout>(R.id.buttonUpdate).setOnClickListener {
                    val appPackageName = activity!!.packageName;
                    try {
                        startActivity(
                            Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                            )
                        )
                    } catch (e: Exception) {
                        Log.d("AAA", e.message!!)
                    }
                }
                dialog.show()
            }
        })
        viewModel.errorLiveData.observe(this, Observer {
            resetButtonLogin()
            buttonLogin.isEnabled = true
            Toast.makeText(
                context,
                resources.getString(R.string.message_internet),
                Toast.LENGTH_SHORT
            ).show()
        })
    }

    @SuppressLint("HardwareIds")
    private fun login() {
        if (CommonUtils.isConnectionInternet(context!!)) {
            when {
                inputLayoutUserName.editText?.length() == 0 -> {
                    inputLayoutUserName.error = ERROR_USERNAME
                    buttonLogin.isEnabled = true
                    resetButtonLogin()
                }
                inputLayoutPassword.editText?.length() == 0 -> {
                    inputLayoutPassword.error = ERROR_PASSWORD
                    buttonLogin.isEnabled = true
                    resetButtonLogin()
                }
                else -> {
                    inputLayoutUserName.isErrorEnabled = false
                    inputLayoutPassword.isErrorEnabled = false
                    Log.d("deviceMac",  Settings.Secure.getString(
                        context!!.contentResolver,
                        Settings.Secure.ANDROID_ID
                    ))
                    viewModel.login(
                        editTextUsername.text.toString(),
                        editTextPassword.text.toString(),
                        version,
                        Settings.Secure.getString(
                            context!!.contentResolver,
                            Settings.Secure.ANDROID_ID
                        )
                    )
                }
            }
        } else {
            resetButtonLogin()
            Toast.makeText(
                activity?.applicationContext,
                resources.getString(R.string.message_internet),
                Toast.LENGTH_SHORT
            ).show()
            return
        }
    }

    private fun animatorButtonLogin() {
        buttonLogin.isEnabled = false
        val handler = Handler()
        handler.postDelayed(object : Runnable {
            override fun run() {
                val animator = ValueAnimator.ofInt(
                    buttonLogin.measuredWidth,
                    resources.getDimension(R.dimen.fix_size).toInt()
                )
                animator.addUpdateListener(object : ValueAnimator.AnimatorUpdateListener {
                    override fun onAnimationUpdate(animation: ValueAnimator?) {
                        val value = animation?.animatedValue as Int
                        val layoutParams = buttonLogin.layoutParams
                        layoutParams.width = value
                        buttonLogin.requestLayout()
                    }
                })
                animator.duration = 100
                animator.start()
            }
        }, 25)
        handler.postDelayed(object : Runnable {
            override fun run() {
                textViewLogin.animate().alpha(0f).start()
            }
        }, 25)
        handler.postDelayed(object : Runnable {
            override fun run() {
                progressbar.visibility = View.VISIBLE
            }
        }, 25)
    }

    private fun resetButtonLogin() {
        buttonLogin.isEnabled = true
        val handler = Handler()
        handler.postDelayed(object : Runnable {
            override fun run() {
                val animator = ValueAnimator.ofInt(
                    buttonLogin.measuredWidth,
                    resources.getDimension(R.dimen.reset_size).toInt()
                )
                animator.addUpdateListener(object : ValueAnimator.AnimatorUpdateListener {
                    override fun onAnimationUpdate(animation: ValueAnimator?) {
                        val value = animation?.animatedValue as Int
                        val layoutParams = buttonLogin.layoutParams
                        layoutParams.width = value
                        buttonLogin.requestLayout()
                    }
                })
                animator.duration = 250
                animator.start()
            }
        }, 25)
        handler.postDelayed(object : Runnable {
            override fun run() {
                textViewLogin.animate().alpha(1f).start()
            }
        }, 25)
        handler.postDelayed(object : Runnable {
            override fun run() {
                progressbar.visibility = View.INVISIBLE
            }
        }, 25)
    }

    private fun showDialog(message: String) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Nội dung cập nhật!")
        builder.setMessage(message)
        builder.setCancelable(false)
        builder.setPositiveButton("Ok") { dialogInterface, i -> dialogInterface.dismiss() }
        val alertDialog = builder.create()
        alertDialog.show()
    }

    companion object {
        val ERROR_USERNAME = "Tên đăng nhập không đúng"
        val ERROR_PASSWORD = "Mật khẩu không đúng"

        fun instance() = LoginFragment()
    }
}
