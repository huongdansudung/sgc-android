package com.coopfoodnewdriscan.scancamera.screen.admin.adapter

import android.graphics.drawable.ColorDrawable
import android.view.View
import com.coopfoodnewdriscan.scancamera.R
import com.coopfoodnewdriscan.scancamera.data.model.UserAdmin
import com.coopfoodnewdriscan.scancamera.utils.BaseAdapter
import com.coopfoodnewdriscan.scancamera.utils.BaseViewHolder
import com.coopfoodnewdriscan.scancamera.utils.OnItemRecyclerViewClickListener
import kotlinx.android.synthetic.main.item_recyclerview_admin_user.view.*
import kotlinx.android.synthetic.main.item_recyclerview_admin_user.view.imageButtonResetLogin

class AdminAdapter: BaseAdapter<UserAdmin>() {
    override fun layout(row: Int): Int = R.layout.item_recyclerview_admin_user

    override fun viewHolder(view: View): BaseViewHolder<UserAdmin> = AdminHolder(view)

    companion object {
        class AdminHolder(
            private val view: View
        ) : BaseViewHolder<UserAdmin>(view) {

            override fun bindData(
                data: UserAdmin,
                listener: OnItemRecyclerViewClickListener<UserAdmin>,
                position: Int
            ) {
                view.textViewUserName.text = data.userName
                view.textViewNumericalOrder.text = position.toString()
                view.imageButtonResetLogin.setOnClickListener { listener.onItemClick(data) }
            }
        }
    }
}