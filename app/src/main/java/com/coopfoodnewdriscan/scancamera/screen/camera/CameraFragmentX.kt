package com.coopfoodnewdriscan.scancamera.screen.camera

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.*
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.hardware.display.DisplayManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.camera.core.*
import androidx.camera.core.ImageCapture.Metadata
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.coopfoodnewdriscan.scancamera.R
import com.coopfoodnewdriscan.scancamera.data.model.TypeBill
import com.coopfoodnewdriscan.scancamera.data.model.User
import com.coopfoodnewdriscan.scancamera.data.source.PhotoRepository
import com.coopfoodnewdriscan.scancamera.data.source.local.PhotoLocalDataSource
import com.coopfoodnewdriscan.scancamera.data.source.local.SharedPrefsApi
import com.coopfoodnewdriscan.scancamera.data.source.remote.PhotoRemoteDataSource
import com.coopfoodnewdriscan.scancamera.screen.camera.CameraActivity.Companion.ANIMATION_FAST_MILLIS
import com.coopfoodnewdriscan.scancamera.screen.camera.CameraActivity.Companion.ANIMATION_SLOW_MILLIS
import com.coopfoodnewdriscan.scancamera.screen.camera.CameraActivity.Companion.KEY_EVENT_ACTION
import com.coopfoodnewdriscan.scancamera.screen.camera.CameraActivity.Companion.KEY_EVENT_EXTRA
import com.coopfoodnewdriscan.scancamera.screen.home.MainActivity
import com.coopfoodnewdriscan.scancamera.screen.selectmutil.MultiPhotoSelectActivity
import com.coopfoodnewdriscan.scancamera.utils.CommonUtils
import com.coopfoodnewdriscan.scancamera.utils.MyViewModelFactory
import com.google.gson.Gson
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.camera_ui_container.*
import kotlinx.android.synthetic.main.fragment_camera.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

/** Helper type alias used for analysis use case callbacks */
typealias LumaListener = (luma: Double) -> Unit

/**
 * Main fragment for this app. Implements all camera operations including:
 * - Viewfinder
 * - Photo taking
 * - Image analysis
 */
class CameraFragmentX : Fragment() {

    private lateinit var container: ConstraintLayout
    private lateinit var viewFinder: PreviewView
    private lateinit var outputDirectory: File
    private lateinit var broadcastManager: LocalBroadcastManager
    private lateinit var user: User
    private lateinit var typeBill: TypeBill
    private lateinit var viewModel: CameraViewModel
    private lateinit var iBatch: String
    private lateinit var progressDialog: AlertDialog

    private var displayId: Int = -1
    private var lensFacing: Int = CameraSelector.LENS_FACING_BACK
    private var preview: Preview? = null
    private var imageCapture: ImageCapture? = null
    private var imageAnalyzer: ImageAnalysis? = null
    private var camera: Camera? = null
    private var cameraProvider: ProcessCameraProvider? = null
    private var iPhoto: Int = 0

    private val displayManager by lazy {
        requireContext().getSystemService(Context.DISPLAY_SERVICE) as DisplayManager
    }

    private lateinit var cameraExecutor: ExecutorService

    private val volumeDownReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.getIntExtra(KEY_EVENT_EXTRA, KeyEvent.KEYCODE_UNKNOWN)) {
                // When the volume down button is pressed, simulate a shutter button click
                /*KeyEvent.KEYCODE_VOLUME_DOWN -> {
                    val shutter = container
                        .findViewById<ImageButton>(R.id.camera_capture_button)
                    shutter.simulateClick()
                }*/
            }
        }
    }

    private val displayListener = object : DisplayManager.DisplayListener {
        override fun onDisplayAdded(displayId: Int) = Unit
        override fun onDisplayRemoved(displayId: Int) = Unit
        override fun onDisplayChanged(displayId: Int) = view?.let { view ->
            if (displayId == this@CameraFragmentX.displayId) {
                Log.d(TAG, "Rotation changed: ${view.display.rotation}")
                imageCapture?.targetRotation = view.display.rotation
                imageAnalyzer?.targetRotation = view.display.rotation
            }
        } ?: Unit
    }

    override fun onDestroyView() {
        super.onDestroyView()
        cameraExecutor.shutdown()
        broadcastManager.unregisterReceiver(volumeDownReceiver)
        displayManager.unregisterDisplayListener(displayListener)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_camera, container, false)

    @SuppressLint("MissingPermission")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        container = view as ConstraintLayout
        viewFinder = container.findViewById(R.id.view_finder)
        iPhoto = arguments!!.getInt(CommonUtils.EXTRA_IPHOTO)
        iBatch = arguments!!.getString(CommonUtils.EXTRA_IBATCH)!!
        user = arguments!!.getParcelable(CommonUtils.EXTRA_USER)!!
        typeBill = arguments!!.getParcelable(CommonUtils.EXTRA_TYPE_BILL)!!
        progressDialog = SpotsDialog.Builder()
            .setContext(context)
            .setCancelable(false)
            .setTheme(R.style.CustomerProgress)
            .build()
        cameraExecutor = Executors.newSingleThreadExecutor()
        broadcastManager = LocalBroadcastManager.getInstance(view.context)
        val filter = IntentFilter().apply { addAction(KEY_EVENT_ACTION) }
        broadcastManager.registerReceiver(volumeDownReceiver, filter)
        displayManager.registerDisplayListener(displayListener, null)
        outputDirectory = CameraActivity.getOutputDirectory(requireContext(), user)
        viewFinder.post {
            displayId = viewFinder.display.displayId
            updateCameraUi()
            setUpCamera()
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        updateCameraUi()
    }

    private fun setUpCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())
        cameraProviderFuture.addListener(Runnable {
            cameraProvider = cameraProviderFuture.get()
            lensFacing = when {
                hasBackCamera() -> CameraSelector.LENS_FACING_BACK
                hasFrontCamera() -> CameraSelector.LENS_FACING_FRONT
                else -> throw IllegalStateException("Back and front camera are unavailable")
            }
            bindCameraUseCases()
        }, ContextCompat.getMainExecutor(requireContext()))
    }

    private fun bindCameraUseCases() {
        val metrics = DisplayMetrics().also { viewFinder.display.getRealMetrics(it) }
        Log.d(TAG, "Screen metrics: ${metrics.widthPixels} x ${metrics.heightPixels}")
        val screenAspectRatio = aspectRatio(metrics.widthPixels, metrics.heightPixels)
        Log.d(TAG, "Preview aspect ratio: $screenAspectRatio")
        val rotation = viewFinder.display.rotation
        val cameraProvider = cameraProvider
            ?: throw IllegalStateException("Camera initialization failed.")
        val cameraSelector = CameraSelector.Builder().requireLensFacing(lensFacing).build()
        preview = Preview.Builder()
            .setTargetRotation(rotation)
            .build()
        imageCapture = ImageCapture.Builder()
            .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
            .setTargetRotation(rotation)
            .build()
        cameraProvider.unbindAll()
        try {
            camera = cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture)
            preview?.setSurfaceProvider(viewFinder.createSurfaceProvider())
        } catch (exc: Exception) {
            Log.e(TAG, "Use case binding failed", exc)
        }
    }

    private fun aspectRatio(width: Int, height: Int): Int {
        val previewRatio = max(width, height).toDouble() / min(width, height)
        if (abs(previewRatio - RATIO_4_3_VALUE) <= abs(previewRatio - RATIO_16_9_VALUE)) {
            return AspectRatio.RATIO_4_3
        }
        return AspectRatio.RATIO_16_9
    }

    @SuppressLint("SetTextI18n")
    private fun updateCameraUi() {
        container.findViewById<ConstraintLayout>(R.id.camera_ui_container)?.let {
            container.removeView(it)
        }
        val controls = View.inflate(requireContext(), R.layout.camera_ui_container, container)
        textViewCustomer.text = user.name
        textViewTypeBill.text = typeBill.nameHD + CommonUtils.TEXT_VIEW_COMPAPY
        if (typeBill.idHD == "9" || typeBill.idHD == "12") {
            checkboxReport.visibility = View.GONE
            textViewDot.visibility = View.GONE
            buttonFinish.visibility = View.GONE
        }
        textViewDot.text = "Kết thúc\n đợt $iBatch"
        initData()
        registerLiveData()
        controls.findViewById<ImageButton>(R.id.buttonCameraCapture).setOnClickListener {
            if (CommonUtils.isConnectionInternet(activity!!.applicationContext)) {
                imageViewSelectPhoto.isEnabled = false
                imageViewBack.isEnabled = false
                checkboxReport.isEnabled = false
                buttonCameraCapture.isEnabled = false
                buttonFinish.isEnabled = false
                imageCapture?.let { imageCapture ->
                    iPhoto++
                    CommonUtils.isReport = checkboxReport.isChecked
                    val photoFile = createFile(outputDirectory, createFileName())
                    val metadata = Metadata().apply {
                        isReversedHorizontal = lensFacing == CameraSelector.LENS_FACING_FRONT
                    }
                    val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile)
                        .setMetadata(metadata)
                        .build()
                    if (typeBill.idHD == "9" || typeBill.idHD == "12") {
                        progressDialog.show()
                    }
                    imageCapture.takePicture(
                        outputOptions, cameraExecutor, object : ImageCapture.OnImageSavedCallback {
                            override fun onError(exc: ImageCaptureException) {
                                Log.e(TAG, "Photo capture failed: ${exc.message}", exc)
                            }

                            override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                                val savedUri = output.savedUri ?: Uri.fromFile(photoFile)
                                Log.d(TAG, "Photo capture succeeded: $savedUri")
                                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                                    requireActivity().sendBroadcast(
                                        Intent(android.hardware.Camera.ACTION_NEW_PICTURE, savedUri)
                                    )
                                }

                                when (typeBill.idHD) {
                                    "9" -> {
                                        viewModel.uploadFileName(photoFile.name)
                                    }
                                    "12" -> {
                                        viewModel.uploadFileName(photoFile.name)
                                    }
                                    else -> {
                                        viewModel.uploadFileName(photoFile.name)
                                    }
                                }
                            }
                        })
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        overlay.background = ColorDrawable(Color.WHITE)
                        overlay.postDelayed(
                            { overlay.background = null },
                            ANIMATION_FAST_MILLIS
                        )
                        container.postDelayed({
                            imageViewSelectPhoto.isEnabled = true
                            imageViewBack.isEnabled = true
                            checkboxReport.isEnabled = true
                            buttonCameraCapture.isEnabled = true
                            buttonFinish.isEnabled = true
                        }, ANIMATION_SLOW_MILLIS)
                    }
                }
                checkboxReport.isChecked = false
            } else {
                Toast.makeText(
                    context,
                    resources.getString(R.string.message_internet),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        controls.findViewById<ImageView>(R.id.imageViewSelectPhoto).setOnClickListener {
            if (CommonUtils.isConnectionInternet(activity!!.applicationContext)) {
                startActivity(MultiPhotoSelectActivity.instance(context!!, user, typeBill))
            } else {
                Toast.makeText(
                    context,
                    resources.getString(R.string.message_internet),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        controls.findViewById<TextView>(R.id.textViewDot).setOnClickListener {
            if (CommonUtils.isConnectionInternet(activity!!.applicationContext)) {
                if (CommonUtils.isConnectionInternet(activity!!.applicationContext)) {
                    val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
                        when (which) {
                            DialogInterface.BUTTON_POSITIVE -> {
                                CommonUtils.timeoutUser = CommonUtils.TIME_OUT_USER_DEFAULT
                                if (CommonUtils.isConnectionInternet(activity!!.applicationContext)) {
                                    viewModel.uploadBatch(iBatch, typeBill.idHD)
                                    constraintLayoutEndBatch.visibility = View.VISIBLE
                                    progressViewEndBatch.visibility = View.VISIBLE
                                } else {
                                    Toast.makeText(
                                        context,
                                        resources.getString(R.string.message_internet),
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }

                            DialogInterface.BUTTON_NEGATIVE -> {
                                CommonUtils.timeoutUser = CommonUtils.TIME_OUT_USER_DEFAULT
                            }
                        }
                    }
                    val builder = AlertDialog.Builder(context)
                    builder.setMessage("Bạn có chắc muốn kết thúc đợt $iBatch không?")
                        .setPositiveButton("Có", dialogClickListener)
                        .setNegativeButton("Không", dialogClickListener).show()
                } else {
                    Toast.makeText(
                        context,
                        resources.getString(R.string.message_internet),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                Toast.makeText(
                    context,
                    resources.getString(R.string.message_internet),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        controls.findViewById<ImageView>(R.id.imageViewBack).setOnClickListener {
            if (CommonUtils.isConnectionInternet(activity!!.applicationContext)) {
                activity!!.onBackPressed()
            } else {
                Toast.makeText(
                    context,
                    resources.getString(R.string.message_internet),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun hasBackCamera(): Boolean {
        return cameraProvider?.hasCamera(CameraSelector.DEFAULT_BACK_CAMERA) ?: false
    }

    private fun hasFrontCamera(): Boolean {
        return cameraProvider?.hasCamera(CameraSelector.DEFAULT_FRONT_CAMERA) ?: false
    }

    private fun initData() {
        val photoRepository = PhotoRepository.instance(
            PhotoLocalDataSource.instance(SharedPrefsApi(context!!, Gson())),
            PhotoRemoteDataSource.instance(SharedPrefsApi(context!!, Gson()))
        )
        viewModel = ViewModelProviders.of(this, MyViewModelFactory(photoRepository))
            .get(CameraViewModel::class.java)
    }

    private fun registerLiveData() {
        viewModel.errorLiveData.observe(this, androidx.lifecycle.Observer {
            if (it.message!!.contains("HTTP 422 UNPROCESSABLE ENTITY")) {
                val intent = Intent(KEY_EVENT_ACTION).apply { putExtra(KEY_EVENT_EXTRA, 111) }
                val intentAc = Intent(context!!, MainActivity::class.java)
                intentAc.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intentAc)
                Toast.makeText(
                    view!!.context,
                    resources.getString(R.string.message_refresh_token_expires),
                    Toast.LENGTH_LONG
                ).show()
                LocalBroadcastManager.getInstance(context!!).sendBroadcast(intent)
            } else {
                Toast.makeText(view!!.context, it!!.message, Toast.LENGTH_LONG).show()
            }
            if (CommonUtils.TYPEBILL.contains("Đính kèm biên bản") ||
                CommonUtils.TYPEBILL.contains("Đính kèm hóa đơn")
            ) {
                progressDialog.dismiss()
                Toast.makeText(
                    context,
                    "Đính kèm thất bại!",
                    Toast.LENGTH_SHORT
                ).show()
                activity!!.onBackPressed()
                CommonUtils.TEXT_VIEW_COMPAPY = ""
            }
        })
        viewModel.uploadBatchLiveData.observe(this, androidx.lifecycle.Observer {
            if (it.message == "Susscess") {
                iPhoto = 0
                iBatch = "" + (iBatch.toInt() + CommonUtils.VALUE_ONE_NUMBER)
                textViewDot.text = "Kết thúc đợt $iBatch"
                constraintLayoutEndBatch.visibility = View.GONE
                progressViewEndBatch.visibility = View.GONE
            } else {
                constraintLayoutEndBatch.visibility = View.GONE
                progressViewEndBatch.visibility = View.GONE
                Toast.makeText(view!!.context, "Kết thúc đợt thất bại !", Toast.LENGTH_LONG).show()
            }
        })
        viewModel.uploadFileNameLiveData.observe(this, androidx.lifecycle.Observer {
            Thread(
                Runnable {
                    val file = File(CommonUtils.navigationFile(user.coopBranch) + it.message)
                    val requestFile = RequestBody.create(
                        "multipart/form-data".toMediaTypeOrNull(),
                        CommonUtils.photoRotation(file.absolutePath)
                    )
                    val body = MultipartBody.Part.createFormData("image", file.name, requestFile)
                    Log.d("AAA", CommonUtils.IDUP)
                    viewModel.uploadFilePhoto(body, CommonUtils.IDUP)
                }
            ).start()
        })
        viewModel.uploadFilePhoto.observe(this, Observer {
            if (CommonUtils.TYPEBILL.contains("Đính kèm biên bản") ||
                CommonUtils.TYPEBILL.contains("Đính kèm hóa đơn")
            ) {
                progressDialog.dismiss()
                Toast.makeText(
                    context,
                    resources.getString(R.string.message_upload_file_attached_successfully),
                    Toast.LENGTH_SHORT
                ).show()
                activity!!.onBackPressed()
                CommonUtils.TEXT_VIEW_COMPAPY = ""
            }
        })
    }

    //yyyyMMdd_HHmmss_IDCus_IDUser_IDHoadon_IDCus_IDcty_Bo_[bienban]_STT.jpg
    private fun createFileName(): String {
        var filename = CommonUtils.getServerDatetime("yyyyMMdd_HHmmss")
        filename = filename + "_" +
                user.idCus + "_" +
                user.id + "_" +
                typeBill.idHD + "_" +
                user.idCus + "_" +
                "1" + "_" +
                String.format("%02d", Integer.parseInt(iBatch)) + "_" +
                checkboxReport.isChecked.toString() + "_" +
                String.format("%04d", iPhoto) +
                PHOTO_EXTENSION
        return filename
    }

    companion object {

        private const val TAG = "CameraXBasic"
        private const val PHOTO_EXTENSION = ".jpg"
        private const val RATIO_4_3_VALUE = 4.0 / 3.0
        private const val RATIO_16_9_VALUE = 16.0 / 9.0

        private fun createFile(baseFolder: File, format: String) = File(baseFolder, format)

        fun instance(user: User, typeBill: TypeBill, iBatch: String, iPhoto: Int) =
            CameraFragmentX().apply {
                val args = Bundle()
                args.putParcelable(CommonUtils.EXTRA_USER, user)
                args.putParcelable(CommonUtils.EXTRA_TYPE_BILL, typeBill)
                args.putString(CommonUtils.EXTRA_IBATCH, iBatch)
                args.putInt(CommonUtils.EXTRA_IPHOTO, iPhoto)
                arguments = args
            }
    }
}
