package com.coopfoodnewdriscan.scancamera.screen.photodetail

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.coopfoodnewdriscan.scancamera.R
import com.coopfoodnewdriscan.scancamera.data.model.PhotoUpload
import com.coopfoodnewdriscan.scancamera.screen.camera.CameraActivity
import com.coopfoodnewdriscan.scancamera.utils.CommonUtils
import com.coopfoodnewdriscan.scancamera.utils.OnItemRecyclerViewClickListener
import kotlinx.android.synthetic.main.activity_photo_detail.*
import java.util.*

class PhotoDetailActivity: AppCompatActivity(), OnItemRecyclerViewClickListener<PhotoUpload> , View.OnClickListener{
    private lateinit var photoUpload: PhotoUpload
    private lateinit var photoUploads : MutableList<PhotoUpload>
    private var position : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_detail)
        initView()
    }

    private val volumeDownReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.getIntExtra(CameraActivity.KEY_EVENT_EXTRA, 111)) {
                111 -> {
                    finish()
                }
            }
        }
    }

    override fun onItemClick(data: PhotoUpload) {
    }

    override fun onClick(v: View?) {
        CommonUtils.timeoutUser = CommonUtils.TIME_OUT_USER_DEFAULT
        when(v!!.id){
            R.id.imageViewBack -> onBackPressed()
        }
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        CommonUtils.timeoutUser = CommonUtils.TIME_OUT_USER_DEFAULT
    }

    @SuppressLint("SetTextI18n")
    private fun initView(){
        val filter = IntentFilter().apply { addAction(CameraActivity.KEY_EVENT_ACTION) }
        LocalBroadcastManager.getInstance(applicationContext).registerReceiver(volumeDownReceiver, filter)
        photoUpload = intent.getParcelableExtra(CommonUtils.EXTRA_PHOTO_UPLOAD)!!
        photoUploads = intent.getParcelableArrayListExtra(CommonUtils.EXTRS_PHOTO_UPLOAD_LIST)!!
        position = intent.getIntExtra(CommonUtils.EXTRAS_POSITION, 0)
        viewPagerViewPhoto.offscreenPageLimit = 0
        val pagerAdapter = MediaPagerAdapter(supportFragmentManager )
        textViewTitlePhoto.text = photoUploads[position].getNumberSerial()
        viewPagerViewPhoto.adapter = pagerAdapter
        viewPagerViewPhoto.currentItem = position
        imageViewBack.setOnClickListener(this)
        viewPagerViewPhoto.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                textViewTitlePhoto.text = photoUploads[position].getNumberSerial()
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    inner class MediaPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
        override fun getCount(): Int = photoUploads.size
        override fun getItem(position: Int): Fragment{
            return PhotoFragment.create(photoUploads[position])
        }
        override fun getItemPosition(obj: Any): Int  = PagerAdapter.POSITION_NONE
    }

    companion object{
        fun instance(context: Context, photoUploads: MutableList<PhotoUpload>, position: Int, photoUpload: PhotoUpload): Intent {
            val intent = Intent(context, PhotoDetailActivity::class.java)
            intent.putParcelableArrayListExtra(CommonUtils.EXTRS_PHOTO_UPLOAD_LIST,photoUploads as ArrayList<PhotoUpload>)
            intent.putExtra(CommonUtils.EXTRA_PHOTO_UPLOAD, photoUpload)
            intent.putExtra(CommonUtils.EXTRAS_POSITION, position)
            return intent
        }
    }
}
