package com.coopfoodnewdriscan.scancamera.screen.statistical

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.coopfoodnewdriscan.scancamera.data.source.PhotoRepository
import com.coopfoodnewdriscan.scancamera.data.source.remote.response.StatisticalPhotoResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class StatisticalViewModel(private val photoRepository: PhotoRepository) : ViewModel(){
    var errorLiveData = MutableLiveData<Throwable>()
    var statisticalLiveData = MutableLiveData<StatisticalPhotoResponse>()
    private val compositeDisposable = CompositeDisposable()

    fun getStatisticalPhoto(datetime: String){
        compositeDisposable.add(
            photoRepository.getStatisticalPhoto(datetime)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { data ->
                        data.let { statisticalLiveData.value = it }
                    }, { error ->
                        error.let { errorLiveData.value = it }
                    }
                )
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
