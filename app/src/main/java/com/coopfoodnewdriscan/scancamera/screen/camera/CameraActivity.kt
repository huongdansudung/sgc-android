package com.coopfoodnewdriscan.scancamera.screen.camera

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.KeyEvent
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.coopfoodnewdriscan.scancamera.R
import com.coopfoodnewdriscan.scancamera.data.model.TypeBill
import com.coopfoodnewdriscan.scancamera.data.model.User
import com.coopfoodnewdriscan.scancamera.utils.CommonUtils
import java.io.File

class CameraActivity : AppCompatActivity() {

    private lateinit var user: User
    private lateinit var typeBill: TypeBill
    private lateinit var iBatch: String
    private lateinit var iPhoto: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)
        iBatch = intent.getStringExtra(CommonUtils.EXTRA_IBATCH)!!
        user = intent.getParcelableExtra(CommonUtils.EXTRA_USER)!!
        typeBill = intent.getParcelableExtra(CommonUtils.EXTRA_TYPE_BILL)!!
        iPhoto = intent.getStringExtra(CommonUtils.EXTRA_IPHOTO)!!
        savedInstanceState ?: supportFragmentManager.beginTransaction()
            .replace(
                R.id.fragment_container,
                CameraFragmentX.instance(user, typeBill, iBatch, iPhoto.toInt())
            )
            .commit()
        val filter = IntentFilter().apply { addAction(KEY_EVENT_ACTION) }
        LocalBroadcastManager.getInstance(applicationContext)
            .registerReceiver(volumeDownReceiver, filter)
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        CommonUtils.timeoutUser = CommonUtils.TIME_OUT_USER_DEFAULT
    }

    private val volumeDownReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.getIntExtra(KEY_EVENT_EXTRA, 111)) {
                111 -> {
                    finish()
                }
            }
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_VOLUME_DOWN -> {
                val intent = Intent(KEY_EVENT_ACTION).apply { putExtra(KEY_EVENT_EXTRA, keyCode) }
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
                true
            }
            else -> super.onKeyDown(keyCode, event)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        CommonUtils.TEXT_VIEW_COMPAPY = ""
    }

    companion object {
        const val KEY_EVENT_ACTION = "key_event_action"
        const val KEY_EVENT_EXTRA = "key_event_extra"
        const val ANIMATION_FAST_MILLIS = 50L
        const val ANIMATION_SLOW_MILLIS = 1000L

        fun getOutputDirectory(context: Context, user: User): File {
            val appContext = context.applicationContext
            val mediaDir = context.externalMediaDirs.firstOrNull()?.let {
                File(CommonUtils.navigationFile(user.coopBranch)).apply { mkdirs() }
            }
            return if (mediaDir != null && mediaDir.exists()) mediaDir else appContext.filesDir
        }

        fun instance(
            context: Context,
            user: User,
            typeBill: TypeBill,
            iBatch: String,
            photoNumber: String
        ): Intent {
            val intent = Intent(context, CameraActivity::class.java)
            intent.putExtra(CommonUtils.EXTRA_USER, user)
            intent.putExtra(CommonUtils.EXTRA_TYPE_BILL, typeBill)
            intent.putExtra(CommonUtils.EXTRA_IBATCH, iBatch)
            intent.putExtra(CommonUtils.EXTRA_IPHOTO, photoNumber)
            return intent
        }
    }
}
