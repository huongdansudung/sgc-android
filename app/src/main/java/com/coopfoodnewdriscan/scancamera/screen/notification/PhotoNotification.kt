package com.coopfoodnewdriscan.scancamera.screen.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.VISIBILITY_PUBLIC
import com.coopfoodnewdriscan.scancamera.R

class PhotoNotification constructor(private val context: Context){
    private lateinit var builder: NotificationCompat.Builder
    private lateinit var notificationManager: NotificationManager
    private lateinit var notificationChannel: NotificationChannel

    fun getBuilder(): NotificationCompat.Builder {
        return builder
    }

    fun initNotification(){
        notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val name = context.getString(R.string.nameChannel)
            val description = context.getString(R.string.descriptionChannel)
            val important = NotificationManager.IMPORTANCE_LOW
            notificationChannel = NotificationChannel(NOTIFICATION_CHANNEL , name, important)
            notificationChannel.description = description
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.GREEN
            notificationManager.createNotificationChannel(notificationChannel)
        }
        builder = NotificationCompat.Builder(context, NOTIFICATION_CHANNEL)
            .setSmallIcon(R.drawable.ic_file_upload)
            .setShowWhen(false)
            .setContentTitle(context.getString(R.string.title_notification))
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setVisibility(VISIBILITY_PUBLIC)
    }

    companion object{
        const val NOTIFICATION_CHANNEL = "dri_upload_photo"
        const val NOTIFICATION_INT_ID = 100
    }
}
