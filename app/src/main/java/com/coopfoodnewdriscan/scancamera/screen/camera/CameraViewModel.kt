package com.coopfoodnewdriscan.scancamera.screen.camera

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.coopfoodnewdriscan.scancamera.data.source.PhotoRepository
import com.coopfoodnewdriscan.scancamera.data.source.remote.response.UploadFilePhoto
import com.coopfoodnewdriscan.scancamera.utils.CommonUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import java.io.File

class CameraViewModel(private val photoRepository: PhotoRepository) : ViewModel() {
    var uploadBatchLiveData = MutableLiveData<UploadFilePhoto>()
    var errorLiveData = MutableLiveData<Throwable>()
    var uploadFileNameLiveData = MutableLiveData<UploadFilePhoto>()
    var uploadFilePhoto = MutableLiveData<UploadFilePhoto>()
    private val compositeDisposable = CompositeDisposable()

    fun uploadFileName(filename: String) {
        compositeDisposable.add(
            photoRepository.uploadFileName(filename)
                .subscribeOn(Schedulers.io())
                .onBackpressureBuffer()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ data ->
                    data.let {
                        uploadFileNameLiveData.value = it
                    }
                }, { error ->
                    error.let {
                        errorLiveData.value = it
                    }
                })
        )
    }

    fun uploadFilePhoto(imageFile: MultipartBody.Part, groupHD: String) {
        val user = photoRepository.loadInformationAccount()
        compositeDisposable.add(
            photoRepository.uploadFilePhoto(imageFile, groupHD)
                .subscribe({ data ->
                    data.let {
                        CommonUtils.IDUP = ""
                        File(CommonUtils.navigationFile(user.coopBranch) + it.message).delete()
                        uploadFilePhoto.postValue(it)
                    }
                }, { error ->
                    error.let { errorLiveData.postValue(it) }
                })
        )
    }

    fun uploadBatch(endBatch: String, typeBill: String) {
        compositeDisposable.add(
            photoRepository.uploadBatch(endBatch, typeBill)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ data ->
                    data.let {
                        uploadBatchLiveData.value = it
                    }
                }, { error ->
                    error.let { errorLiveData.value = it }
                })
        )
    }

//    override fun onCleared() {
//        super.onCleared()
//       // compositeDisposable.clear()
//    }
}
