package com.coopfoodnewdriscan.scancamera.screen.selectmutil.adapter

import android.view.View
import com.coopfoodnewdriscan.scancamera.R
import com.coopfoodnewdriscan.scancamera.data.model.PhotoUpload
import com.coopfoodnewdriscan.scancamera.utils.BaseAdapter
import com.coopfoodnewdriscan.scancamera.utils.BaseViewHolder
import com.coopfoodnewdriscan.scancamera.utils.OnItemRecyclerViewClickListener
import com.coopfoodnewdriscan.scancamera.utils.loadImageUrl
import kotlinx.android.synthetic.main.item_recyclerview_photo.view.*

class PhotoAdapter : BaseAdapter<PhotoUpload>() {
    override fun layout(row: Int): Int = R.layout.item_recyclerview_photo

    override fun viewHolder(view: View): BaseViewHolder<PhotoUpload> = PhotoHolder(view)

    companion object {
        class PhotoHolder(
            private val view: View
        ) : BaseViewHolder<PhotoUpload>(view) {

            override fun bindData(
                data: PhotoUpload,
                listener: OnItemRecyclerViewClickListener<PhotoUpload>,
                position: Int
            ) {
                view.textViewFileName.text = data.imageName
                view.setOnClickListener { listener.onItemClick(data) }
            }
        }
    }
}
