package com.coopfoodnewdriscan.scancamera.screen.admin

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.coopfoodnewdriscan.scancamera.data.source.PhotoRepository
import com.coopfoodnewdriscan.scancamera.data.source.remote.response.ResetMacResponse
import com.coopfoodnewdriscan.scancamera.data.source.remote.response.UserAdminResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class AdminViewModel(private val photoRepository: PhotoRepository) : ViewModel() {
    var errorLiveData = MutableLiveData<Throwable>()
    var resetMacLiveData = MutableLiveData<ResetMacResponse>()
    var userAdminLiveData = MutableLiveData<UserAdminResponse>()
    private val compositeDisposable = CompositeDisposable()

    fun resetMac(userId: String) {
        compositeDisposable.add(
            photoRepository.resetMac(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ data ->
                    data.let {
                        resetMacLiveData.value = it
                    }
                }, { error ->
                    error.let { errorLiveData.value = it }
                })
        )
    }

    fun userAdmin(){
        compositeDisposable.add(
            photoRepository.userAdmins()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ data ->
                    data.let {
                        userAdminLiveData.value = it
                    }
                }, { error ->
                    error.let { errorLiveData.value = it }
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
