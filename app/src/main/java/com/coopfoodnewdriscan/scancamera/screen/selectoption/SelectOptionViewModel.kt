package com.coopfoodnewdriscan.scancamera.screen.selectoption

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.coopfoodnewdriscan.scancamera.data.model.ServerDatetime
import com.coopfoodnewdriscan.scancamera.data.source.PhotoRepository
import com.coopfoodnewdriscan.scancamera.data.source.remote.response.BillsResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class SelectOptionViewModel(private val photoRepository: PhotoRepository) : ViewModel() {
    private val compositeDisposable = CompositeDisposable()
    var getBillsLiveData = MutableLiveData<BillsResponse>()
    var serverDateLiveData = MutableLiveData<ServerDatetime>()
    var errorLiveData = MutableLiveData<Throwable>()
    var errorGetTimeLiveData = MutableLiveData<Throwable>()

    fun getBills() {
        compositeDisposable.add(
            photoRepository.getBills()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ data ->
                    data.let {
                        getBillsLiveData.value = it
                    }
                }, { error ->
                    error.let {
                        errorLiveData.value = it
                    }
                })
        )
    }

    fun getServerDatetime(){
        compositeDisposable.add(
            photoRepository.getServerDate()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ data ->
                    data.let {
                        serverDateLiveData.value = it
                    }
                }, { error ->
                    error.let {
                        errorGetTimeLiveData.value = it
                    }
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
