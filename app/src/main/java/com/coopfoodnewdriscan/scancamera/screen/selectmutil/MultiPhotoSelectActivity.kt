package com.coopfoodnewdriscan.scancamera.screen.selectmutil

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.DividerItemDecoration
import com.coopfoodnewdriscan.scancamera.R
import com.coopfoodnewdriscan.scancamera.data.model.PhotoUpload
import com.coopfoodnewdriscan.scancamera.data.model.TypeBill
import com.coopfoodnewdriscan.scancamera.data.model.User
import com.coopfoodnewdriscan.scancamera.data.source.PhotoRepository
import com.coopfoodnewdriscan.scancamera.data.source.local.PhotoLocalDataSource
import com.coopfoodnewdriscan.scancamera.data.source.local.SharedPrefsApi
import com.coopfoodnewdriscan.scancamera.data.source.remote.PhotoRemoteDataSource
import com.coopfoodnewdriscan.scancamera.screen.camera.CameraActivity
import com.coopfoodnewdriscan.scancamera.screen.home.MainActivity
import com.coopfoodnewdriscan.scancamera.screen.login.LoginViewModel
import com.coopfoodnewdriscan.scancamera.screen.photodetail.PhotoDetailActivity
import com.coopfoodnewdriscan.scancamera.screen.selectmutil.adapter.PhotoAdapter
import com.coopfoodnewdriscan.scancamera.utils.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_mutil_photo_select.*
import kotlinx.android.synthetic.main.activity_mutil_photo_select.toolbar
import kotlinx.android.synthetic.main.fragment_select_bill.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MultiPhotoSelectActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener,
    OnItemRecyclerViewClickListener<PhotoUpload>
    , View.OnClickListener {
    private lateinit var user: User
    private lateinit var typeBill: TypeBill
    private lateinit var spinnerBatchUploadedAdapter: ArrayAdapter<String>
    private lateinit var spinnerBatchAdapter: ArrayAdapter<String>
    private lateinit var batchs: ArrayList<String>
    private lateinit var batchsUploaded: ArrayList<String>
    private lateinit var viewModel: LoginViewModel
    private lateinit var photoUploads: ArrayList<PhotoUpload>
    private lateinit var photoUploadsCurrent: MutableList<PhotoUpload>
    private var menuItem: MenuItem? = null
    private var page = 1
    private val photoAdapter: PhotoAdapter by lazy { PhotoAdapter() }
    private var batch: Int = 0
    private var uploadMenuItemTwo: MenuItem? = null
    private var notUploadedMenuItem: MenuItem? = null
    private var uploadMenuItem: MenuItem? = null
    private var isUpload: Boolean = false
    private var isTypeMenu: Int = 1
    private var dateTime: String = ""
    private var isActionBack: Boolean = true
    private var indexBatch: Int = 1

    //first load activity
    private var isFirstLoad = false
    private lateinit var dialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mutil_photo_select)
        initView()
        initData()
        registerLiveData()
        val filter = IntentFilter().apply { addAction(CameraActivity.KEY_EVENT_ACTION) }
        LocalBroadcastManager.getInstance(applicationContext)
            .registerReceiver(volumeDownReceiver, filter)
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        CommonUtils.timeoutUser = CommonUtils.TIME_OUT_USER_DEFAULT
    }

    private val volumeDownReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.getIntExtra(CameraActivity.KEY_EVENT_EXTRA, 111)) {
                // When the volume down button is pressed, simulate a shutter button click
                111 -> {
                    finish()
                }
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        indexBatch = position
        if (isFirstLoad) {
            if (!isUpload) {
                batch = position
                loadPhotoLocal(menuItem!!.title.toString().replace("/", ""), batch.toString())
            } else {
                batch = position + CommonUtils.VALUE_ONE_NUMBER
                page = CommonUtils.VALUE_ONE_NUMBER
                viewModel.getPhotoUpload(
                    typeBill.idHD,
                    menuItem!!.title.toString().replace("/", ""),
                    batch.toString(),
                    page.toString()
                )
                progressView.visibility = View.VISIBLE
                photoAdapter.clearList()
            }
        } else {
            loadPhotoLocal(menuItem!!.title.toString().replace("/", ""), "0")
            isFirstLoad = true
        }
    }

    override fun onClick(v: View?) {
        CommonUtils.timeoutUser = CommonUtils.TIME_OUT_USER_DEFAULT
    }

    @SuppressLint("SimpleDateFormat")
    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        if (isTypeMenu == 1) {
            menuItem = menu.findItem(R.id.action_date)
            if (isActionBack) {
                menuItem!!.title = CommonUtils.getServerDatetime("yyyy/MM/dd")
            } else {
                menuItem!!.title = dateTime
            }
            viewModel.getBatch(menuItem!!.title.toString().replace("/", ""), typeBill.idHD)
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (isTypeMenu == 1) {
            menuInflater.inflate(R.menu.menu_status_photo, menu)
            uploadMenuItemTwo = menu!!.findItem(R.id.actionUploadTwo)
            notUploadedMenuItem = menu.findItem(R.id.actionNotUpload)
            uploadMenuItem = menu.findItem(R.id.actionUpload)
            notUploadedMenuItem!!.isChecked = true
        } else {
            menuInflater.inflate(R.menu.menu_reupload, menu)
        }
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        CommonUtils.isReUploadHand = false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.actionCamera -> this.finish()
            R.id.actionSelectDate -> pickDate(menuItem!!)
            R.id.actionUploadTwo -> {
                if (!isUpload) {
                    //set up spinner
                    spinnerBatch.adapter = null
                    spinnerBatch.adapter = spinnerBatchUploadedAdapter
                    spinnerBatchAdapter.notifyDataSetChanged()
                    spinnerBatch.setSelection(indexBatch - 1)
                }

                isUpload = true

                //set up menu
                uploadMenuItem!!.setIcon(R.drawable.ic_action_uploaded)
                uploadMenuItemTwo!!.isChecked = true
                notUploadedMenuItem!!.isChecked = false

            }
            R.id.actionNotUpload -> {

                if (isUpload) {
                    //set up spinner
                    spinnerBatch.adapter = null
                    spinnerBatch.adapter = spinnerBatchAdapter
                    spinnerBatchAdapter.notifyDataSetChanged()
                    spinnerBatch.setSelection(indexBatch + 1)
                }

                isUpload = false

                //set up menu
                uploadMenuItem!!.setIcon(R.drawable.ic_action_not_upload)
                uploadMenuItemTwo!!.isChecked = false
                notUploadedMenuItem!!.isChecked = true
            }
            R.id.actionReUpload -> {
                if (!isUpload) {
                    isTypeMenu = 2
                    dateTime = menuItem!!.title.toString()
                    invalidateOptionsMenu()
                }
            }
            R.id.actionBack -> {
                CommonUtils.isReUploadHand = false
                isActionBack = false
                isUpload = false
                isTypeMenu = 1
                page = CommonUtils.VALUE_ONE_NUMBER
                uploadMenuItem!!.setIcon(R.drawable.ic_action_uploaded)
                uploadMenuItemTwo!!.isChecked = false
                notUploadedMenuItem!!.isChecked = true
                loadPhotoLocal(menuItem!!.title.toString().replace("/", ""), batch.toString())
                invalidateOptionsMenu()
            }
            R.id.actionUploads -> {
                if (CommonUtils.isConnectionInternet(applicationContext)) {
                    loadList(batch.toString())
                    if (photoUploads.isNotEmpty()) {
                        if (!CommonUtils.isReUploadService) {
                            CommonUtils.isReUploadHand = true
                            constraintLayoutReUpload.visibility = View.VISIBLE
                            progressView.visibility = View.VISIBLE
                            viewModel.uploadFileName(photoUploads.first().imageName)
                        } else {
                            Toast.makeText(
                                applicationContext,
                                "Service đang úp ảnh không thể úp ảnh thủ công.\n Xin hãy thử lại lần sau! ",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    } else {
                        Toast.makeText(
                            applicationContext,
                            "Không có ảnh nào để úp !",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    Toast.makeText(
                        applicationContext,
                        "Không tìm thấy server, vui lòng kiểm tra lại internet",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onItemClick(data: PhotoUpload) {
        var position = 0
        for (i in 0 until photoUploadsCurrent.size) {
            if (photoUploadsCurrent[i].imageName == data.imageName) {
                position = i
            }
        }
        startActivity(
            PhotoDetailActivity.instance(
                applicationContext,
                photoUploadsCurrent,
                position,
                data
            )
        )
    }

    private fun initView() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        user = intent.getParcelableExtra(CommonUtils.EXTRA_USER)!!
        typeBill = intent.getParcelableExtra(CommonUtils.EXTRA_TYPE_BILL)!!
        dialog = Dialog(this)
        dialog.setContentView(R.layout.custom_dialog_update_app)
        photoUploads = ArrayList()
        textViewCustomer.text = user.name
        textViewTypeBill.text = typeBill.nameHD
        batchs = ArrayList()
        batchsUploaded = ArrayList()
        spinnerBatchAdapter =
            ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, batchs)
        spinnerBatchAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerBatchUploadedAdapter =
            ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, batchsUploaded)
        spinnerBatchUploadedAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerBatch.adapter = spinnerBatchAdapter
        recyclerViewPhotoUpload.setHasFixedSize(true)
        val dividerItemDecoration = DividerItemDecoration(applicationContext, DividerItemDecoration.VERTICAL)
        recyclerViewPhotoUpload.addItemDecoration(dividerItemDecoration)
        recyclerViewPhotoUpload.adapter = photoAdapter
        spinnerBatch.onItemSelectedListener = this
        photoAdapter.setOnItemClickListener(this)
    }

    private fun initData() {
        val photoRepository = PhotoRepository.instance(
            PhotoLocalDataSource.instance(
                SharedPrefsApi(
                    applicationContext,
                    Gson()
                )
            ), PhotoRemoteDataSource.instance(SharedPrefsApi(this, Gson()))
        )
        viewModel = ViewModelProviders.of(this, MyViewModelFactory(photoRepository))
            .get(LoginViewModel::class.java)
        recyclerViewPhotoUpload.addOnScrollListener(object : EndlessScrollListener() {
            override fun onLoadMore() {
                if (isUpload) {
                    page++
                    viewModel.getPhotoUpload(
                        typeBill.idHD,
                        menuItem!!.title.toString().replace("/", ""),
                        batch.toString(),
                        page.toString()
                    )
                }
            }
        })
    }

    private fun registerLiveData() {
        viewModel.batchLiveData.observe(this, Observer {
            if (it.message == "yes") {
                dialog.setCancelable(false)
                dialog.window!!.setBackgroundDrawable(ColorDrawable(0))
                dialog.findViewById<ImageView>(R.id.imageViewCancel).setOnClickListener { finish() }
                dialog.findViewById<FrameLayout>(R.id.buttonUpdate).setOnClickListener {
                    val appPackageName = packageName
                    try {
                        startActivity(
                            Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                            )
                        )
                    } catch (e: Exception) {
                        Log.d("AAA", e.message!!)
                    }
                }
                dialog.show()
            } else {
                batchs.clear()
                batchsUploaded.clear()
                batchs.add("All photo")
                for (i in 1..it.batch.toInt()) {
                    batchs.add("Đợt $i")
                    batchsUploaded.add("Đợt $i")
                    spinnerBatchAdapter.notifyDataSetChanged()
                }
            }
        })
        viewModel.errorLiveData.observe(this, Observer {
            if (it.message!!.contains("HTTP 422 UNPROCESSABLE ENTITY")) {
                val intent = Intent(CameraActivity.KEY_EVENT_ACTION).apply {
                    putExtra(
                        CameraActivity.KEY_EVENT_EXTRA,
                        111
                    )
                }
                val intentAc = Intent(applicationContext, MainActivity::class.java)
                intentAc.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intentAc)
                Toast.makeText(
                    applicationContext,
                    resources.getString(R.string.message_refresh_token_expires),
                    Toast.LENGTH_LONG
                ).show()
                LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
            } else {
                Toast.makeText(applicationContext, it!!.message, Toast.LENGTH_LONG).show()
            }
        })
        viewModel.photoUploadLiveData.observe(this, Observer {
            textViewCountPhoto.text = it.totalPhoto + " Ảnh"
            progressView.visibility = View.GONE
            if (it.totalPhoto == "0") {
                constantLayoutNoImage.visibility = View.VISIBLE
            } else {
                constantLayoutNoImage.visibility = View.GONE
                if (it.photoUploads[0].imageName != "") {
                    photoAdapter.addItems(it.photoUploads)
                    photoUploadsCurrent = photoAdapter.getList()
                    photoUploads = it.photoUploads as ArrayList<PhotoUpload>
                }
            }
        })
        viewModel.reUploadFilePhoto.observe(this, Observer {
            if (photoUploads.isNotEmpty()) {
                viewModel.uploadFileName(photoUploads[0].imageName)
            } else {
                constraintLayoutReUpload.visibility = View.GONE
                progressView.visibility = View.GONE
                photoAdapter.clearList()
                textViewCountPhoto.text = resources.getString(R.string.count_photo_default)
                Toast.makeText(
                    applicationContext,
                    resources.getString(R.string.message_reupload_success),
                    Toast.LENGTH_SHORT
                ).show()
                CommonUtils.isReUploadHand = false
            }
        })
        viewModel.reUploadFileName.observe(this, Observer {
            if (it.message != "Error") {
                Thread(
                    Runnable {
                        val file =
                            File(CommonUtils.navigationFile(user.coopBranch) + photoUploads.first().imageName)
                        val requestFile = RequestBody.create(
                            "multipart/form-data".toMediaTypeOrNull(),
                            CommonUtils.photoRotation(file.absolutePath)
                        )
                        val body =
                            MultipartBody.Part.createFormData("image", file.name, requestFile)
                        viewModel.uploadFilePhoto(body, CommonUtils.IDUP)
                        photoUploads.removeAt(0)
                        if (photoUploads.isNotEmpty()) {
                            photoAdapter.removeItem(photoUploads.first())
                        }
                    }
                ).start()
            }
        })
    }

    @SuppressLint("SimpleDateFormat")
    private fun pickDate(menuItem: MenuItem) {
        val calendar = Calendar.getInstance()
        val now = Calendar.getInstance()
        val day = CommonUtils.day
        val month = CommonUtils.month
        val year = CommonUtils.year
        val datePickerDialog = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                CommonUtils.timeoutUser = CommonUtils.TIME_OUT_USER_DEFAULT
                val simpleDateFormat = SimpleDateFormat("yyyy/MM/dd")
                calendar.set(year, month, dayOfMonth)
                now.set(CommonUtils.year, CommonUtils.month - 1, CommonUtils.day)
                if (calendar.time.after(now.time)) {
                    Toast.makeText(
                        applicationContext,
                        resources.getString(R.string.error_date),
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    menuItem.title = simpleDateFormat.format(calendar.time)
                    viewModel.getBatch(menuItem.title.toString().replace("/", ""), typeBill.idHD)
                    if (isUpload) {
                        progressView.visibility = View.VISIBLE
                        isUpload = true
                        page = CommonUtils.VALUE_ONE_NUMBER
                        uploadMenuItem!!.setIcon(R.drawable.ic_action_uploaded)
                        uploadMenuItemTwo!!.isChecked = true
                        notUploadedMenuItem!!.isChecked = false
                        viewModel.getPhotoUpload(
                            typeBill.idHD,
                            simpleDateFormat.format(calendar.time).toString().replace("/", ""),
                            batch.toString(),
                            page.toString()
                        )
                        photoAdapter.clearList()
                        photoUploads.clear()
                    } else {
                        isUpload = false
                        loadPhotoLocal(
                            simpleDateFormat.format(calendar.time).toString().replace("/", ""),
                            batch.toString()
                        )
                    }
                }
            }, year, month - 1, day
        )
        datePickerDialog.show()
    }

    @SuppressLint("SetTextI18n")
    private fun loadPhotoLocal(dateTime: String, batch: String) {
        textViewCountPhoto.text = resources.getString(R.string.count_photo_default)
        var countPhoto = 0
        photoUploads.clear()
        photoAdapter.clearList()
        val file = File(CommonUtils.navigationFile(user.coopBranch))
        if (file.exists()) {
            val files = file.listFiles()
            if (files.isNotEmpty()) {
                for (file in files) {
                    val arrayName = file.name.split("_")
                    if (arrayName[4] == typeBill.idHD && arrayName[0] == dateTime) {
                        countPhoto++
                    }
                }
                if (countPhoto > 0) {
                    var countPhotoByBatch = 0
                    for (file in files) {
                        val arrayName = file.name.split("_")
                        val timePhoto = file.name.replace("_", "").substring(0, 14).toDouble()
                        val timeNow = CommonUtils.getServerDatetime("yyyyMMddHHmmss").toDouble()
                        if (batch != "0") {
                            if (arrayName[4] == typeBill.idHD &&
                                arrayName[7] == String.format("%02d", Integer.parseInt(batch)) &&
                                arrayName[0] == dateTime &&
                                ((timeNow - timePhoto) > 500)
                            ) {
                                photoUploads.add(
                                    PhotoUpload(
                                        file.name,
                                        file.absolutePath,
                                        file.absolutePath
                                    )
                                )
                                photoAdapter.replaceItem(photoUploads)
                                countPhotoByBatch++
                            }
                        } else {
                            if (arrayName[4] == typeBill.idHD && arrayName[0] == dateTime && ((timeNow - timePhoto) > 500)) {
                                photoUploads.add(
                                    PhotoUpload(
                                        file.name,
                                        file.absolutePath,
                                        file.absolutePath
                                    )
                                )
                                photoAdapter.replaceItem(photoUploads)
                                countPhotoByBatch++
                            }
                        }
                    }
                    photoUploadsCurrent = photoAdapter.getList()
                    textViewCountPhoto.text = "$countPhotoByBatch Ảnh"
                    if (countPhotoByBatch <= 0) {
                        constantLayoutNoImage.visibility = View.VISIBLE
                    } else {
                        constantLayoutNoImage.visibility = View.GONE
                    }
                } else {
                    Toast.makeText(
                        applicationContext,
                        "Không có ảnh nào của loại hóa đơn này !",
                        Toast.LENGTH_LONG
                    ).show()
                    constantLayoutNoImage.visibility = View.VISIBLE
                }
            } else {
                Toast.makeText(
                    applicationContext,
                    "Không có ảnh nào trong máy !",
                    Toast.LENGTH_LONG
                ).show()
                constantLayoutNoImage.visibility = View.VISIBLE
            }
        } else {
            textViewCountPhoto.text = resources.getString(R.string.count_photo_default)
            Toast.makeText(applicationContext, "Không có ảnh nào trong máy !", Toast.LENGTH_LONG)
                .show()
            constantLayoutNoImage.visibility = View.VISIBLE
        }
    }

    //dateTime.replace("/", "")
    private fun loadList(batch: String) {
        textViewCountPhoto.text = resources.getString(R.string.count_photo_default)
        var countPhoto = 0
        photoUploads.clear()
        photoAdapter.clearList()
        val file = File(CommonUtils.navigationFile(user.coopBranch))
        if (file.exists()) {
            val files = file.listFiles()
            if (files.isNotEmpty()) {
                for (file in files) {
                    val arrayName = file.name.split("_")
                    if (arrayName[4] == typeBill.idHD && arrayName[0] == dateTime.replace(
                            "/",
                            ""
                        )
                    ) {
                        countPhoto++
                    }
                }
                if (countPhoto > 0) {
                    var countPhotoByBatch = 0
                    for (file in files) {
                        val arrayName = file.name.split("_")
                        val timePhoto = file.name.replace("_", "").substring(0, 14).toDouble()
                        val timeNow = CommonUtils.getServerDatetime("yyyyMMddHHmmss").toDouble()
                        if (batch != "0") {
                            if (arrayName[4] == typeBill.idHD &&
                                arrayName[7] == String.format("%02d", Integer.parseInt(batch)) &&
                                arrayName[0] == dateTime.replace("/", "") &&
                                ((timeNow - timePhoto) > 500)
                            ) {
                                photoUploads.add(
                                    PhotoUpload(
                                        file.name,
                                        file.absolutePath,
                                        file.absolutePath
                                    )
                                )
                                photoUploads.sort()
                                photoAdapter.replaceItem(photoUploads)
                                countPhotoByBatch++
                            }
                        } else {
                            if (arrayName[4] == typeBill.idHD &&
                                arrayName[0] == dateTime.replace("/", "") &&
                                ((timeNow - timePhoto) > 500)
                            ) {
                                photoUploads.add(
                                    PhotoUpload(
                                        file.name,
                                        file.absolutePath,
                                        file.absolutePath
                                    )
                                )
                                photoUploads.sort()
                                photoAdapter.replaceItem(photoUploads)
                                countPhotoByBatch++
                            }
                        }

                    }
                    textViewCountPhoto.text = "$countPhotoByBatch Ảnh"
                } else {
                    Toast.makeText(
                        applicationContext,
                        "Không có ảnh nào của thư mục này!",
                        Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                Toast.makeText(
                    applicationContext,
                    "Không có ảnh nào trong máy !",
                    Toast.LENGTH_LONG
                ).show()
            }
        } else {
            textViewCountPhoto.text = resources.getString(R.string.count_photo_default)
            Toast.makeText(applicationContext, "Không có ảnh nào trong máy !", Toast.LENGTH_LONG)
                .show()
        }
    }

    companion object {
        fun instance(context: Context, user: User, typeBill: TypeBill): Intent {
            val intent = Intent(context, MultiPhotoSelectActivity::class.java)
            intent.putExtra(CommonUtils.EXTRA_USER, user)
            intent.putExtra(CommonUtils.EXTRA_TYPE_BILL, typeBill)
            return intent
        }
    }
}
