package com.coopfoodnewdriscan.scancamera.screen.statistical

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.coopfoodnewdriscan.scancamera.R
import com.coopfoodnewdriscan.scancamera.data.model.TypeBill
import com.coopfoodnewdriscan.scancamera.data.model.User
import com.coopfoodnewdriscan.scancamera.data.source.PhotoRepository
import com.coopfoodnewdriscan.scancamera.data.source.local.PhotoLocalDataSource
import com.coopfoodnewdriscan.scancamera.data.source.local.SharedPrefsApi
import com.coopfoodnewdriscan.scancamera.data.source.remote.PhotoRemoteDataSource
import com.coopfoodnewdriscan.scancamera.screen.camera.CameraActivity
import com.coopfoodnewdriscan.scancamera.screen.home.MainActivity
import com.coopfoodnewdriscan.scancamera.screen.statistical.adapter.StatisticalAdapter
import com.coopfoodnewdriscan.scancamera.utils.CommonUtils
import com.coopfoodnewdriscan.scancamera.utils.MyViewModelFactory
import com.coopfoodnewdriscan.scancamera.utils.OnItemRecyclerViewClickListener
import com.github.dewinjm.monthyearpicker.MonthYearPickerDialog
import com.github.dewinjm.monthyearpicker.MonthYearPickerDialogFragment
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_statistical.*

class StatisticalActivity : AppCompatActivity(), View.OnClickListener,
    OnItemRecyclerViewClickListener<TypeBill> {
    private var yearSelect = 0
    private var monthSelect = 0
    private val statisticalAdapter: StatisticalAdapter by lazy { StatisticalAdapter() }
    private lateinit var monthYearPickerDialogFragment: MonthYearPickerDialogFragment
    private lateinit var viewModel: StatisticalViewModel
    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statistical)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = resources.getString(R.string.title_statistical)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        initView()
        initData()
        registerLiveData()
        pickDateTime()
        val filter = IntentFilter().apply { addAction(CameraActivity.KEY_EVENT_ACTION) }
        LocalBroadcastManager.getInstance(applicationContext)
            .registerReceiver(volumeDownReceiver, filter)
    }

    private val volumeDownReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.getIntExtra(CameraActivity.KEY_EVENT_EXTRA, 111)) {
                // When the volume down button is pressed, simulate a shutter button click
                111 -> {
                    finish()
                }
            }
        }
    }

    override fun onClick(v: View?) {
        CommonUtils.timeoutUser = CommonUtils.TIME_OUT_USER_DEFAULT
        when (v!!.id) {
            R.id.imageViewDatetime -> {
                pickDateTime()
            }
        }
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        CommonUtils.timeoutUser = CommonUtils.TIME_OUT_USER_DEFAULT
    }

    override fun onItemClick(data: TypeBill) {

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initView() {
        user = intent.getParcelableExtra(CommonUtils.EXTRA_USER)!!
        recyclerViewStatistical.addItemDecoration(
            DividerItemDecoration(
                this,
                LinearLayoutManager.VERTICAL
            )
        )
        recyclerViewStatistical.adapter = statisticalAdapter
        imageViewDatetime.setOnClickListener(this)
        statisticalAdapter.setOnItemClickListener(this)
    }

    private fun initData() {
        val photoRepository = PhotoRepository.instance(
            PhotoLocalDataSource.instance(
                SharedPrefsApi(
                    applicationContext,
                    Gson()
                )
            ), PhotoRemoteDataSource.instance(SharedPrefsApi(this, Gson()))
        )
        viewModel = ViewModelProviders.of(this, MyViewModelFactory(photoRepository)).get(
            StatisticalViewModel::class.java
        )
    }

    @SuppressLint("SetTextI18n")
    private fun registerLiveData() {
        viewModel.statisticalLiveData.observe(this, androidx.lifecycle.Observer {
            statisticalAdapter.replaceItem(it.typeBills)
            textViewSumPhoto.text =
                resources.getString(R.string.title_total_photo) + totalPhoto(it.typeBills).toString()
            progressView.visibility = View.GONE
        })
        viewModel.errorLiveData.observe(this, androidx.lifecycle.Observer {
            if (it.message!!.contains("HTTP 422 UNPROCESSABLE ENTITY")) {
                val intent = Intent(CameraActivity.KEY_EVENT_ACTION).apply {
                    putExtra(
                        CameraActivity.KEY_EVENT_EXTRA,
                        111
                    )
                }
                val intentAc = Intent(applicationContext, MainActivity::class.java)
                intentAc.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intentAc)
                Toast.makeText(
                    applicationContext,
                    resources.getString(R.string.message_refresh_token_expires),
                    Toast.LENGTH_LONG
                ).show()
                LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
            } else {
                Toast.makeText(applicationContext, it!!.message, Toast.LENGTH_LONG).show()
            }
            progressView.visibility = View.GONE
        })
    }

    private fun pickDateTime() {
        yearSelect = CommonUtils.year
        monthSelect = CommonUtils.month
        monthYearPickerDialogFragment = MonthYearPickerDialogFragment.getInstance(
            monthSelect - CommonUtils.VALUE_ONE_NUMBER,
            yearSelect
        )
        monthYearPickerDialogFragment.isCancelable = false
        monthYearPickerDialogFragment.show(supportFragmentManager, null)
        monthYearPickerDialogFragment.setOnDateSetListener(object :
            MonthYearPickerDialog.OnDateSetListener {
            @SuppressLint("SetTextI18n")
            override fun onDateSet(year: Int, monthOfYear: Int) {
                CommonUtils.timeoutUser = CommonUtils.TIME_OUT_USER_DEFAULT
                if (monthOfYear + CommonUtils.VALUE_ONE_NUMBER > monthSelect || year > yearSelect) {
                    Toast.makeText(
                        applicationContext,
                        resources.getString(R.string.error_date_time),
                        Toast.LENGTH_LONG
                    ).show()
                    pickDateTime()
                } else {
                    monthSelect = monthOfYear + CommonUtils.VALUE_ONE_NUMBER
                    yearSelect = year
                    textViewDatetime.text = "Tháng: $monthSelect Năm: $yearSelect"
                    viewModel.getStatisticalPhoto(
                        yearSelect.toString().substring(2, 4) + String.format("%02d", monthSelect)
                    )
                    progressView.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun totalPhoto(typeBills: List<TypeBill>): Int {
        var countPhoto = 0
        for (bill in typeBills) {
            countPhoto += bill.totalPhoto!!.toInt()
        }
        return countPhoto
    }

    companion object {
        fun instance(context: Context, user: User): Intent {
            val intent = Intent(context, StatisticalActivity::class.java)
            intent.putExtra(CommonUtils.EXTRA_USER, user)
            return intent
        }
    }
}
