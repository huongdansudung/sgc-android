package com.coopfoodnewdriscan.scancamera.screen.selectoption

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.coopfoodnewdriscan.scancamera.R
import com.coopfoodnewdriscan.scancamera.data.model.User
import com.coopfoodnewdriscan.scancamera.data.source.PhotoRepository
import com.coopfoodnewdriscan.scancamera.data.source.local.PhotoLocalDataSource
import com.coopfoodnewdriscan.scancamera.data.source.local.SharedPrefsApi
import com.coopfoodnewdriscan.scancamera.data.source.remote.PhotoRemoteDataSource
import com.coopfoodnewdriscan.scancamera.screen.camera.CameraActivity
import com.coopfoodnewdriscan.scancamera.screen.home.MainActivity
import com.coopfoodnewdriscan.scancamera.screen.selectbill.SelectBillFragment
import com.coopfoodnewdriscan.scancamera.screen.service.ServicePhoto
import com.coopfoodnewdriscan.scancamera.screen.statistical.StatisticalActivity
import com.coopfoodnewdriscan.scancamera.utils.CommonUtils
import com.coopfoodnewdriscan.scancamera.utils.MyViewModelFactory
import com.coopfoodnewdriscan.scancamera.utils.replaceFragmentToFragment
import com.google.gson.Gson
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.fragment_select_option.*
import java.text.SimpleDateFormat
import java.util.*

class SelectOptionFragment : Fragment() , View.OnClickListener{
    private lateinit var user: User
    private lateinit var viewModel: SelectOptionViewModel
    private lateinit var progressDialog: AlertDialog
    private lateinit var dialog: Dialog
    private var isGetBills = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_select_option, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initData()
        registerLiveData()
    }

    override fun onClick(v: View?) {
        CommonUtils.timeoutUser = CommonUtils.TIME_OUT_USER_DEFAULT
        when(v!!.id){
            R.id.imageViewDashBoard -> {
                if (CommonUtils.isConnectionInternet(activity!!.applicationContext)) {
                    startActivity(StatisticalActivity.instance(context!!, user))
                }else {
                    Toast.makeText(
                        context,
                        resources.getString(R.string.message_internet),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            R.id.imageViewTakePhoto -> {
                if (CommonUtils.isConnectionInternet(activity!!.applicationContext)) {
                    viewModel.getBills()
                    isGetBills = true
                }else {
                    Toast.makeText(
                        context,
                        resources.getString(R.string.message_internet),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun initView() {
        user = arguments!!.getParcelable(CommonUtils.ACTION_USER)!!
        progressDialog = SpotsDialog.Builder()
            .setContext(context)
            .setCancelable(false)
            .setTheme(R.style.CustomerProgress)
            .build()
        textViewBranch.text = user.name
        textViewUserName.text = user.userName
        dialog = Dialog(context!!)
        dialog.setContentView(R.layout.custom_dialog_timeout)
        imageViewDashBoard.setOnClickListener(this)
        imageViewTakePhoto.setOnClickListener(this)
    }

    private fun initData() {
        val photoRepository = PhotoRepository.instanceOption(
            PhotoLocalDataSource.instance(SharedPrefsApi(context!!, Gson())),
            PhotoRemoteDataSource.instanceOption(SharedPrefsApi(context!!, Gson()))
        )
        viewModel = ViewModelProviders.of(this, MyViewModelFactory(photoRepository))
            .get(SelectOptionViewModel::class.java)
        viewModel.getServerDatetime()
        progressDialog.show()
    }

    @SuppressLint("SimpleDateFormat")
    private fun registerLiveData() {
        viewModel.getBillsLiveData.observe(this, Observer {
            if (isGetBills){
                isGetBills = false
                replaceFragmentToFragment(R.id.layoutContainer, SelectBillFragment.instance(user, it.bills), true)
            }
        })
        viewModel.errorLiveData.observe(this, Observer {
            if (it.message!!.contains("HTTP 422 UNPROCESSABLE ENTITY")){
                val intent = Intent(CameraActivity.KEY_EVENT_ACTION).apply { putExtra(
                    CameraActivity.KEY_EVENT_EXTRA, 111) }
                val intentAc = Intent(context!!, MainActivity::class.java)
                intentAc.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intentAc)
                Toast.makeText(view!!.context,  resources.getString(R.string.message_refresh_token_expires), Toast.LENGTH_LONG).show()
                LocalBroadcastManager.getInstance(context!!).sendBroadcast(intent)
            }else {
                Toast.makeText(view!!.context, it!!.message, Toast.LENGTH_LONG).show()
            }
        })
        viewModel.serverDateLiveData.observe(this, Observer {
            if (it.message == "ok") {
                progressDialog.dismiss()
                val calendar = Calendar.getInstance()
                val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                calendar.time = sdf.parse(it.datetime)
                val currentTime = Calendar.getInstance().time
                val serverTime = calendar.time
                CommonUtils.timeDifference = currentTime.time - serverTime.time

                val intentService = ServicePhoto.serviceInstance(context!!, user)
                if (!isMyServiceRunning(ServicePhoto::class.java)) {
                    activity!!.startService(intentService)
                }
                val array = it.datetime.split(" ")
                if (!CommonUtils.isDialogTimeout) {
                    checkDatetime(array[0].split("-")[0].toInt(), array[0].split("-")[1].toInt(), array[0].split("-")[2].toInt())
                    CommonUtils.isDialogTimeout = true
                }
            }else if (it.message == "error") {
                viewModel.getServerDatetime()
            }
        })
        viewModel.errorGetTimeLiveData.observe(this, Observer {
            viewModel.getServerDatetime()
        })
    }

    @SuppressLint("SetTextI18n")
    private fun checkDatetime(yearServer: Int, monthServer: Int, dayServer: Int){
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH) + CommonUtils.VALUE_ONE_NUMBER
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        if (year != yearServer || month != monthServer || day != dayServer){
            dialog.setCancelable(false)
            dialog.findViewById<ImageView>(R.id.imageViewIconTick).setImageDrawable(resources.getDrawable(R.drawable.clock_warning))
            dialog.findViewById<TextView>(R.id.textViewTitle).text = "Cảnh Báo!"
            dialog.findViewById<TextView>(R.id.textViewMessage).text = "Thời gian của máy không chính xác: $day/$month/$year, ${calendar.get(Calendar.HOUR_OF_DAY)}:${ String.format("%02d", calendar.get(Calendar.MINUTE))}."
            dialog.window!!.setBackgroundDrawable(ColorDrawable(0))
            dialog.findViewById<Button>(R.id.buttonOK).setOnClickListener{ dialog.cancel() }
            dialog.show()
        }
    }

    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = activity!!.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    companion object{
        fun instance(user: User) = SelectOptionFragment().apply {
            val args = Bundle()
            args.putParcelable(CommonUtils.ACTION_USER, user)
            arguments = args
        }
    }
}
