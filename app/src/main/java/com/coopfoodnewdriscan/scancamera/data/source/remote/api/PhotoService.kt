package com.coopfoodnewdriscan.scancamera.data.source.remote.api

import com.coopfoodnewdriscan.scancamera.data.model.AccessToken
import com.coopfoodnewdriscan.scancamera.data.model.Batch
import com.coopfoodnewdriscan.scancamera.data.model.ServerDatetime
import com.coopfoodnewdriscan.scancamera.data.model.User
import com.coopfoodnewdriscan.scancamera.data.source.remote.response.*
import com.google.gson.JsonObject
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.http.*

interface PhotoService {
    @POST("logincoop")
    @FormUrlEncoded
    fun login(@Field("username") userName: String,
              @Field("password") password: String,
              @Field("version") version: String,
              @Field("deviceMac") deviceMac: String): Observable<User>

    @GET("in_app_update")
    fun inAppUpdate(@Query("version") version: String): Observable<InAppUpdateResponse>

    @GET("list_HD")
    fun getBills(): Observable<BillsResponse>

    @GET("batch")
    fun getBatch(@Query("date") date: String,
                 @Query("typeInvoice") typeInvoice: String): Observable<Batch>

    @POST("refresh")
    @FormUrlEncoded
    fun refreshTokenTest(@Field("refreshToken") refreshToken: String): Single<AccessToken>

    @POST("upload-filename")
    @FormUrlEncoded
    fun uploadFileName(@Field("imageFilename") filename: String): Flowable<UploadFilePhoto>

    @POST("upload-image")
    @Multipart
    fun uploadFilePhoto(@Part pic: MultipartBody.Part,
                        @Part("groupHD") groupHD: String): Observable<UploadFilePhoto>

    @POST("ktt_hddt")
    @FormUrlEncoded
    fun uploadBatch(@Field("endBatch") endBatch: String ,
                    @Field("typeInvoice") typeBill: String): Observable<UploadFilePhoto>

    @POST("view_status")
    @FormUrlEncoded
        fun getPhotoUpload(@Field("typeInvoice") typeInvoice: String,
                       @Field("date") datetime: String,
                       @Field("batch") batch: String,
                       @Field("page") page: String): Observable<PhotoUploadResponse>

    @GET("view_status_month")
    fun getStatisticalPhoto(@Query("date") datetime: String): Observable<StatisticalPhotoResponse>

    @GET("server-datetime")
    fun getServerData(): Observable<ServerDatetime>

    @POST("reset_mac")
    @FormUrlEncoded
    fun resetMac(@Field("userReset") userId: String): Observable<ResetMacResponse>

    @GET("user-admins")
    fun userAdmins(): Observable<UserAdminResponse>
}
