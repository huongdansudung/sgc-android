package com.coopfoodnewdriscan.scancamera.data.source

import com.coopfoodnewdriscan.scancamera.data.model.AccessToken
import com.coopfoodnewdriscan.scancamera.data.model.Batch
import com.coopfoodnewdriscan.scancamera.data.model.ServerDatetime
import com.coopfoodnewdriscan.scancamera.data.model.User
import com.coopfoodnewdriscan.scancamera.data.source.local.PhotoLocalDataSource
import com.coopfoodnewdriscan.scancamera.data.source.remote.PhotoRemoteDataSource
import com.coopfoodnewdriscan.scancamera.data.source.remote.response.*
import io.reactivex.Flowable
import io.reactivex.Observable
import okhttp3.MultipartBody

class PhotoRepository private constructor(
    private val local: PhotoLocalDataSource,
    private val remote: PhotoRemoteDataSource
) {

    fun inAppUpdate(version: String): Observable<InAppUpdateResponse> {
        return remote.inAppUpdate(version)
    }

    fun resetMac(userId: String): Observable<ResetMacResponse> {
        return remote.resetMac(userId)
    }

    fun getServerDate(): Observable<ServerDatetime> {
        return remote.getServerDatetime()
    }

    fun getBills(): Observable<BillsResponse> {
        return remote.getBills()
    }

    fun login(
        username: String,
        password: String,
        version: String,
        deviceMac: String
    ): Observable<User> {
        return remote.login(username, password, version, deviceMac)
    }

    fun getBatch(date: String, typeInvoice: String): Observable<Batch> {
        return remote.getBatch(date, typeInvoice)
    }

    fun uploadFileName(filename: String): Flowable<UploadFilePhoto> {
        return remote.uploadFileName(filename)
    }

    fun uploadFilePhoto(imageFile: MultipartBody.Part, groupHD: String): Observable<UploadFilePhoto> {
        return remote.uploadFilePhoto(imageFile, groupHD)
    }

    fun uploadBatch(endBatch: String, typeBill: String): Observable<UploadFilePhoto> {
        return remote.uploadBatch(endBatch, typeBill)
    }

    fun getPhotoUpload(
        typeBill: String,
        datetime: String,
        batch: String,
        page: String
    ): Observable<PhotoUploadResponse> {
        return remote.getPhotoUpload(typeBill, datetime, batch, page)
    }

    fun getStatisticalPhoto(datetime: String): Observable<StatisticalPhotoResponse> {
        return remote.getStatisticalPhoto(datetime)
    }

    fun loadInformationAccount(): User {
        return local.loadInformationAccount()
    }

    fun saveInformationAccount(user: User, userName: String, password: String) {
        return local.saveInformationAccount(user, userName, password)
    }

    fun saveAccessToken(accessToken: AccessToken) {
        return local.saveAccessToken(accessToken)
    }

    fun userAdmins(): Observable<UserAdminResponse>{
        return remote.userAdmins()
    }

    companion object {
        private var sInstance: PhotoRepository? = null

        @JvmStatic
        fun instance(
            local: PhotoLocalDataSource,
            remote: PhotoRemoteDataSource
        ): PhotoRepository {
            if (sInstance == null) {
                synchronized(this) {
                    sInstance = PhotoRepository(local, remote)
                }
            }
            return sInstance!!
        }

        fun instanceOption(
            local: PhotoLocalDataSource,
            remote: PhotoRemoteDataSource
        ): PhotoRepository {
            sInstance = PhotoRepository(local, remote)
            return sInstance!!
        }
    }
}
