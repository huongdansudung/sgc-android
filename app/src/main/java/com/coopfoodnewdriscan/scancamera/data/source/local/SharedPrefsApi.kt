package com.coopfoodnewdriscan.scancamera.data.source.local

import android.content.Context
import android.content.SharedPreferences
import com.coopfoodnewdriscan.scancamera.data.model.AccessToken
import com.google.gson.Gson

class SharedPrefsApi(context: Context, private val gson: Gson) {

    private val mSharedPreferences: SharedPreferences

    init {
        mSharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    }

    operator fun <T> get(key: String, clazz: Class<T>): T? {
        if (clazz == String::class.java) {
            return mSharedPreferences.getString(key, DEFAULT_STRING_VALUE) as T?
        } else if (clazz == Boolean::class.java) {
            return java.lang.Boolean.valueOf(mSharedPreferences.getBoolean(key, false)) as T
        } else if (clazz == Float::class.java) {
            return java.lang.Float.valueOf(mSharedPreferences.getFloat(key, DEFAULT_NUMBER_VALUE.toFloat())) as T
        } else if (clazz == Int::class.java) {
            return Integer.valueOf(mSharedPreferences.getInt(key, DEFAULT_NUMBER_VALUE)) as T
        } else if (clazz == Long::class.java) {
            return java.lang.Long.valueOf(mSharedPreferences.getLong(key, DEFAULT_NUMBER_VALUE.toLong())) as T
        } else if (clazz == AccessToken::class.java) {
            return gson.fromJson(mSharedPreferences.getString(key, null), clazz) as T
        }
        return null
    }

    fun <T> put(key: String, data: T) {
        val editor = mSharedPreferences.edit()
        if (data is String) {
            editor.putString(key, data as String)
        } else if (data is Boolean) {
            editor.putBoolean(key, data as Boolean)
        } else if (data is Float) {
            editor.putFloat(key, data as Float)
        } else if (data is Int) {
            editor.putInt(key, data as Int)
        } else if (data is Long) {
            editor.putLong(key, data as Long)
        } else if (data is AccessToken) {
            editor.putString(key, gson.toJson(data))
        }
        editor.apply()
    }

    fun clear() {
        mSharedPreferences.edit().clear().apply()
    }

    companion object {
        private val PREFS_NAME = "PreferencesAccount"
        private val DEFAULT_NUMBER_VALUE = 0
        private val DEFAULT_STRING_VALUE = ""
    }
}
