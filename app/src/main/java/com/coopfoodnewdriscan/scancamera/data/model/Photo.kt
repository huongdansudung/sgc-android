package com.coopfoodnewdriscan.scancamera.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "Photo")
class Photo {
    @PrimaryKey
    var id: Int = 0
}
