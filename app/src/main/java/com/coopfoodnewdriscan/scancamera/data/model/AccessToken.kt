package com.coopfoodnewdriscan.scancamera.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AccessToken(
    @SerializedName("token")
    var token: String,
    @SerializedName("refreshToken")
    var refreshToken: String,
    @SerializedName("message")
    var message: String
): Parcelable
