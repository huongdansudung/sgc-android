package com.coopfoodnewdriscan.scancamera.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TypeBill(
    @SerializedName("id")
    var idHD: String,
    @SerializedName("nameBill")
    var nameHD: String,
    @SerializedName("totalphoto")
    var totalPhoto: Int = 0
) : Parcelable
