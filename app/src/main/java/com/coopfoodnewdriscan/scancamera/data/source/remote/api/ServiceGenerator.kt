package com.coopfoodnewdriscan.scancamera.data.source.remote.api

import com.coopfoodnewdriscan.scancamera.BuildConfig
import com.coopfoodnewdriscan.scancamera.data.source.local.SharedPrefsApi
import com.coopfoodnewdriscan.scancamera.data.source.middleware.TokenAuthenticator
import com.coopfoodnewdriscan.scancamera.data.source.middleware.TokenInterceptor
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ServiceGenerator {
    //118.69.176.18:5007 server test
    //210.2.93.45:5014 server Coop Food
    //210.2.93.45:5013 server Coop Mart
    companion object {
        private const val READ_TIMEOUT = 100000
        private const val WRITE_TIMEOUT = 100000
        private const val CONNECT_TIMEOUT = 100000
        private var sInstance: PhotoService? = null
        private var apiBaseUrl = "http://210.2.93.45:5020/"

        private var builder = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(apiBaseUrl)

        fun create(sharedPrefsApi: SharedPrefsApi): PhotoService {
            val tokenInterceptor = TokenInterceptor(sharedPrefsApi)
            val tokenAuthenticator = TokenAuthenticator(sharedPrefsApi)
            val logging = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC)!!
            val httpClient: OkHttpClient.Builder = OkHttpClient.Builder()
                .readTimeout(READ_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
                .writeTimeout(WRITE_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
                .connectTimeout(CONNECT_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
                .authenticator(tokenAuthenticator)
                .addInterceptor(tokenInterceptor)
                .retryOnConnectionFailure(true)
            if (BuildConfig.DEBUG) {
                httpClient.addInterceptor(logging)
            }
            val retrofit = builder.client(httpClient.build()).build()
            return retrofit.create(PhotoService::class.java)
        }

        fun changeApiBaseUrl(newApiBaseUrl: String) {
            apiBaseUrl = newApiBaseUrl
            builder = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(apiBaseUrl)
        }

        fun instance(sharedPrefsApi: SharedPrefsApi): PhotoService {
            if (sInstance == null) {
                synchronized(this) {
                    sInstance = create(sharedPrefsApi)
                }
            }
            return sInstance!!
        }

        fun instanceOption(sharedPrefsApi: SharedPrefsApi): PhotoService {
            sInstance = create(sharedPrefsApi)
            return sInstance!!
        }
    }
}
