package com.coopfoodnewdriscan.scancamera.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ServerDatetime(
    @SerializedName("datetime")
    var datetime: String,
    @SerializedName("message")
    var message: String
): Parcelable