package com.coopfoodnewdriscan.scancamera.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PhotoUpload(
    @SerializedName("imagename")
    var imageName:String,
    @SerializedName("url")
    var url: String,
    @SerializedName("urlthumb")
    var urlthumb:String
): Parcelable, Comparable<PhotoUpload>{

    override fun compareTo(other: PhotoUpload): Int {
        val timePhoto = imageName.replace("_", "").substring(0, 14)
        return when {
            timePhoto.toDouble() == other.imageName.replace("_", "").substring(0, 14).toDouble() -> 0
            timePhoto.toDouble() > other.imageName.replace("_", "").substring(0, 14).toDouble() -> 1
            else -> -1
        }
    }

    fun getNumberSerial(): String{
        return if (imageName.isNotEmpty()){
            imageName.split("_")[9].substring(0,4)
        }else ""
    }
}
