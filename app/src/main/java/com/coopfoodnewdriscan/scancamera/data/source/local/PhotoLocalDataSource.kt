package com.coopfoodnewdriscan.scancamera.data.source.local

import com.coopfoodnewdriscan.scancamera.data.model.AccessToken
import com.coopfoodnewdriscan.scancamera.data.model.User
import com.coopfoodnewdriscan.scancamera.data.source.PhotoDataSource

class PhotoLocalDataSource private constructor(private val sharedPrefsApi: SharedPrefsApi) :
    PhotoDataSource.PhotoLocalDataSource {

    override fun loadInformationAccount(): User {
        val id = sharedPrefsApi[SharedPrefsKey.ID, String::class.java]
        val center = sharedPrefsApi[SharedPrefsKey.CENTER, String::class.java]
        val idCus = sharedPrefsApi[SharedPrefsKey.IDCUS, String::class.java]
        val name = sharedPrefsApi[SharedPrefsKey.NAME, String::class.java]
        val userName = sharedPrefsApi[SharedPrefsKey.USERNAMEPRE, String::class.java]
        val password = sharedPrefsApi[SharedPrefsKey.PASSWORDPRE, String::class.java]
        val coopBranch = sharedPrefsApi[SharedPrefsKey.COOP_BRANCH, String::class.java]
        val idcusNew = sharedPrefsApi[SharedPrefsKey.IDCUS_NEW, String::class.java]
        return User(id!!, center!!,"", idCus!!, name!!, "", null, "", "", userName!!, password!!,coopBranch!!, idcusNew!!)
    }

    override fun saveInformationAccount(user: User, userName: String, password: String) {
        sharedPrefsApi.put(SharedPrefsKey.ID, user.id)
        sharedPrefsApi.put(SharedPrefsKey.CENTER, user.center)
        sharedPrefsApi.put(SharedPrefsKey.IDCUS, user.idCus)
        sharedPrefsApi.put(SharedPrefsKey.NAME, user.name)
        sharedPrefsApi.put(SharedPrefsKey.USERNAMEPRE, userName)
        sharedPrefsApi.put(SharedPrefsKey.PASSWORDPRE, password)
        sharedPrefsApi.put(SharedPrefsKey.COOP_BRANCH, user.coopBranch)
        sharedPrefsApi.put(SharedPrefsKey.IDCUS_NEW, user.idCus_new)
    }

    override fun saveAccessToken(accessToken: AccessToken) {
        sharedPrefsApi.put(SharedPrefsKey.ACCESS_TOKEN, accessToken)
    }

    companion object {
        private var sInstance: PhotoLocalDataSource? = null

        @JvmStatic
        fun instance(sharedPrefsApi: SharedPrefsApi): PhotoLocalDataSource {
            synchronized(this) {
                if (sInstance == null) {
                    sInstance = PhotoLocalDataSource(sharedPrefsApi)
                }
            }
            return sInstance!!
        }
    }
}
