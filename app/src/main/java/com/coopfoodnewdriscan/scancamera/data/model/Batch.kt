package com.coopfoodnewdriscan.scancamera.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Batch(
    @SerializedName("batch")
    var batch: String,
    @SerializedName("Info")
    var message: String,
    @SerializedName("numberPhoto")
    var photoNumber: String
): Parcelable
