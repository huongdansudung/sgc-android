package com.coopfoodnewdriscan.scancamera.data.source.middleware

import com.coopfoodnewdriscan.scancamera.data.model.AccessToken
import com.coopfoodnewdriscan.scancamera.data.source.local.SharedPrefsApi
import com.coopfoodnewdriscan.scancamera.data.source.local.SharedPrefsKey
import com.coopfoodnewdriscan.scancamera.data.source.remote.api.ServiceGenerator
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route

class TokenAuthenticator(private val sharedPrefsApi: SharedPrefsApi) : Authenticator {
    override fun authenticate(route: Route?, response: Response): Request? {
        val photoService = ServiceGenerator.instance(sharedPrefsApi)
        val newAccessToken =
            photoService.refreshTokenTest(sharedPrefsApi[SharedPrefsKey.ACCESS_TOKEN, AccessToken::class.java]!!.refreshToken)
                .blockingGet()
        sharedPrefsApi.put(SharedPrefsKey.ACCESS_TOKEN, newAccessToken)
        return response.request.newBuilder()
            .header("Authorization", "Bearer ${newAccessToken.token}")
            .build()
    }

    private fun responseCount(response: Response): Int {
        var response = response
        var result = 1
        while (response.priorResponse.also { response = it!! } != null) {
            result++
        }
        return result
    }
}
