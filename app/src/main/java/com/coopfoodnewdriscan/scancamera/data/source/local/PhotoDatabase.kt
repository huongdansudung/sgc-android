package com.coopfoodnewdriscan.scancamera.data.source.local

import android.content.Context
import androidx.room.RoomDatabase

abstract class PhotoDatabase : RoomDatabase() {

    abstract fun photoDAO(): PhotoDAO

    companion object {
        private var sPhotoDatabase: PhotoDatabase? = null

        fun instance(context: Context): PhotoDatabase {
            if (sPhotoDatabase == null) {

            }
            return sPhotoDatabase as PhotoDatabase
        }
    }
}
