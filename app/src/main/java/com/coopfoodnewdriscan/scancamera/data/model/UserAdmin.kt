package com.coopfoodnewdriscan.scancamera.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class UserAdmin(
    @SerializedName("userid")
    var userId: String,
    @SerializedName("username")
    var userName: String
) : Parcelable
