package com.coopfoodnewdriscan.scancamera.data.source.remote.response

import android.os.Parcelable
import com.coopfoodnewdriscan.scancamera.data.model.TypeBill
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BillsResponse(
    @SerializedName("image")
    var image: String,
    @SerializedName("result")
    var bills: List<TypeBill>
): Parcelable
