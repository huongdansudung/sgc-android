package com.coopfoodnewdriscan.scancamera.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "PhotoNotUpload")
data class PhotoNotUpLoad(
    @PrimaryKey
    var fileName:String,
    var userName:String?,
    var report:String?,
    var batch: String?,
    var groupId: String?,
    var proof: String?,
    var photoInstead: String?,
    var groupHd:String
):Parcelable
