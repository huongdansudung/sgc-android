package com.coopfoodnewdriscan.scancamera.data.source.remote.response

import android.os.Parcelable
import com.coopfoodnewdriscan.scancamera.data.model.UserAdmin
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserAdminResponse(
    @SerializedName("message")
    var message: String,
    @SerializedName("Role")
    var role: String,
    @SerializedName("list_user")
    var listUser: List<UserAdmin>
) : Parcelable