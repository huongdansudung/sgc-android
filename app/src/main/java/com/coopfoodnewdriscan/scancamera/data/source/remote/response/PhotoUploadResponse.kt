package com.coopfoodnewdriscan.scancamera.data.source.remote.response

import android.os.Parcelable
import com.coopfoodnewdriscan.scancamera.data.model.PhotoUpload
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PhotoUploadResponse(
    @SerializedName("tonganh")
    var totalPhoto: String,
    @SerializedName("results")
    var photoUploads: List<PhotoUpload>
): Parcelable
