package com.coopfoodnewdriscan.scancamera.data.source

import com.coopfoodnewdriscan.scancamera.data.model.AccessToken
import com.coopfoodnewdriscan.scancamera.data.model.Batch
import com.coopfoodnewdriscan.scancamera.data.model.ServerDatetime
import com.coopfoodnewdriscan.scancamera.data.model.User
import com.coopfoodnewdriscan.scancamera.data.source.remote.response.*
import com.google.gson.JsonObject
import io.reactivex.Flowable
import io.reactivex.Observable
import okhttp3.MultipartBody

interface PhotoDataSource {
    interface PhotoLocalDataSource {
        fun saveInformationAccount(user: User, userName: String, password: String)
        fun loadInformationAccount(): User
        fun saveAccessToken(accessToken: AccessToken)
    }

    interface PhotoRemoteDataSource {
        fun login(username: String, password: String, version: String,deviceMac: String): Observable<User>
        fun getBatch(date: String, typeInvoice: String): Observable<Batch>
        fun uploadFileName(filename: String): Flowable<UploadFilePhoto>
        fun uploadFilePhoto(imageFile: MultipartBody.Part, groupHD: String): Observable<UploadFilePhoto>
        fun uploadBatch(endBatch: String, typeBill: String): Observable<UploadFilePhoto>
        fun getPhotoUpload(typeBill: String ,datetime: String, batch: String, page: String): Observable<PhotoUploadResponse>
        fun getStatisticalPhoto(datetime: String): Observable<StatisticalPhotoResponse>
        fun getBills(): Observable<BillsResponse>
        fun getServerDatetime(): Observable<ServerDatetime>
        fun resetMac(userId: String): Observable<ResetMacResponse>
        fun inAppUpdate(version: String): Observable<InAppUpdateResponse>
        fun userAdmins(): Observable<UserAdminResponse>
    }
}
