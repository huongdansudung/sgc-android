package com.coopfoodnewdriscan.scancamera.data.source.local

object SharedPrefsKey {
    const val USERNAMEPRE = "Username"
    const val PASSWORDPRE = "Password"
    const val ID = "ID"
    const val IDCUS = "IDCUS"
    const val NAME = "NAME"
    const val CENTER = "CENTER"
    const val CHECKCOOP = "checkCoop"
    const val ACCESS_TOKEN = "ACCESS_TOKEN"
    const val COOP_BRANCH = "coopBranch"
    const val IDCUS_NEW = "idCus_new"
}
