package com.coopfoodnewdriscan.scancamera.data.source.remote.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResetMacResponse(
    @SerializedName("message")
    var message: String
): Parcelable