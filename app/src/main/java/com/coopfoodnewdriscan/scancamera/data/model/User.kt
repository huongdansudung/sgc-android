package com.coopfoodnewdriscan.scancamera.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    @SerializedName("id")
    var id: String,
    @SerializedName("center")
    var center: String,
    @SerializedName("message")
    var message: String,
    @SerializedName("idCus")
    var idCus: String,
    @SerializedName("name")
    var name: String,
    @SerializedName("role")
    var role: String,
    @SerializedName("list_user")
    var userAdmins: List<UserAdmin>?,
    @SerializedName("refreshToken")
    var refreshToken: String,
    @SerializedName("token")
    var token: String,
    var userName: String,
    var password: String,
    @SerializedName("coopBranch")
    var coopBranch: String,
    //Sửa test
    @SerializedName("idCus_new")
    var idCus_new: String
) : Parcelable
