package com.coopfoodnewdriscan.scancamera.data.source.remote

import com.coopfoodnewdriscan.scancamera.data.model.Batch
import com.coopfoodnewdriscan.scancamera.data.model.ServerDatetime
import com.coopfoodnewdriscan.scancamera.data.model.User
import com.coopfoodnewdriscan.scancamera.data.source.PhotoDataSource
import com.coopfoodnewdriscan.scancamera.data.source.local.SharedPrefsApi
import com.coopfoodnewdriscan.scancamera.data.source.remote.api.PhotoService
import com.coopfoodnewdriscan.scancamera.data.source.remote.api.ServiceGenerator
import com.coopfoodnewdriscan.scancamera.data.source.remote.response.*
import io.reactivex.Flowable
import io.reactivex.Observable
import okhttp3.MultipartBody

class PhotoRemoteDataSource private constructor(private val photoService: PhotoService) :
    PhotoDataSource.PhotoRemoteDataSource {
    override fun getStatisticalPhoto(datetime: String): Observable<StatisticalPhotoResponse> {
        return photoService.getStatisticalPhoto(datetime)
    }

    override fun getBills(): Observable<BillsResponse> {
        return photoService.getBills()
    }

    override fun getServerDatetime(): Observable<ServerDatetime> {
        return photoService.getServerData()
    }

    override fun resetMac(userId: String): Observable<ResetMacResponse> {
        return photoService.resetMac(userId)
    }

    override fun inAppUpdate(version: String): Observable<InAppUpdateResponse> {
        return photoService.inAppUpdate(version)
    }

    override fun getPhotoUpload(
        typeBill: String,
        datetime: String,
        batch: String,
        page: String
    ): Observable<PhotoUploadResponse> {
        return photoService.getPhotoUpload(typeBill, datetime, batch, page)
    }

    override fun uploadBatch(
        endBatch: String,
        typeBill: String
    ): Observable<UploadFilePhoto> {
        return photoService.uploadBatch(endBatch, typeBill)
    }

    override fun uploadFilePhoto(
        imageFile: MultipartBody.Part,
        groupHD: String
    ): Observable<UploadFilePhoto> {
        return photoService.uploadFilePhoto(imageFile, groupHD)
    }

    override fun uploadFileName(filename: String): Flowable<UploadFilePhoto> {
        return photoService.uploadFileName(filename)
    }

    override fun getBatch(
        date: String, typeInvoice: String
    ): Observable<Batch> {
        return photoService.getBatch(date, typeInvoice)
    }

    override fun login(
        username: String,
        password: String,
        version: String,
        deviceMac: String
    ): Observable<User> {
        return photoService.login(username, password, version, deviceMac)
    }

    override fun userAdmins(): Observable<UserAdminResponse> {
        return photoService.userAdmins()
    }

    companion object {
        private var sInstance: PhotoRemoteDataSource? = null

        @JvmStatic
        fun instance(sharedPrefsApi: SharedPrefsApi): PhotoRemoteDataSource {
            if (sInstance == null) {
                synchronized(this) {
                    sInstance = PhotoRemoteDataSource(ServiceGenerator.instance(sharedPrefsApi))
                }
            }
            return sInstance!!
        }

        fun instanceOption(sharedPrefsApi: SharedPrefsApi): PhotoRemoteDataSource {
            sInstance = PhotoRemoteDataSource(ServiceGenerator.instanceOption(sharedPrefsApi))
            return sInstance!!
        }

    }
}
