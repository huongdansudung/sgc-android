package com.coopfoodnewdriscan.scancamera.data.source.remote.response

import android.os.Parcelable
import com.coopfoodnewdriscan.scancamera.data.model.TypeBill
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class StatisticalPhotoResponse(
    @SerializedName("results")
    var typeBills: List<TypeBill>
): Parcelable
